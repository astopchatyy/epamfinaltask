﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("listenings")]
    [ApiController]
    [Authorize(Roles = "user")]
    public class ListeningController : ControllerBase
    {
        private readonly ILotListeningService _listeningService;

        public ListeningController(ILotListeningService listeningService)
        {
            _listeningService = listeningService;
        }
        [HttpPost]
        public async Task<IActionResult> AddListener([FromBody] LotListeningModel model)
        {
            if (model is null)
                return BadRequest("Creation request is empty");
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (model.UserName != requestUserName)
            {
                return Forbid();
            }

            await _listeningService.AddListener(model);

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveListener([FromBody] LotListeningModel model)
        {
            if (model is null)
                return BadRequest("Remove request is empty");
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (model.UserName != requestUserName)
            {
                return Forbid();
            }

            await _listeningService.RemoveListener(model);

            return Ok();
        }
    }
}

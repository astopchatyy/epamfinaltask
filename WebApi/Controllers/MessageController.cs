﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("messages")]
    [ApiController]
    [Authorize]
    public class MessageController
        : ControllerBase
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpGet]
        public async Task<IActionResult> GetMessagesForUser()
        {
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;

            if (requestUserName is null)
                return Forbid();

            List<MessageModel> messages = (await _messageService.GetMessagesByUserNameAsync(requestUserName)).ToList();

            return Ok(messages);
        }
        [HttpGet("{messageId}")]
        public async Task<IActionResult> GetMessage(int messageId)
        {
            MessageModel message = await _messageService.GetByIdAsync(messageId);

            if (message is null)
                return NotFound("Message was deleted");

            return Ok(message);
        }
        [HttpDelete("{messageId}")]
        public async Task<IActionResult> DeleteMessage(int messageId)
        {
            await _messageService.DeleteByIdAsync(messageId);
            return Ok();
        }
        [HttpPut("{messageId}")]
        public async Task<IActionResult> MarkAsRead(int messageId)
        {
            await _messageService.MarkAsReadAsync(messageId);
            return Ok();
        }
    }
}

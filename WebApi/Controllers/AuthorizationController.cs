﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    public class AuthorizationController : ControllerBase
    {
        private readonly IAuthorizationService _authorizationService;


        public AuthorizationController(IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }


        [HttpPost("signin")]
        public async Task<IActionResult> LogIn([FromBody] LogInModel model)
        {
            if (model is null)
                return BadRequest("LogIn data is empty");

            AuctionUserModel user = await _authorizationService.LogInAsync(model);

            if (model is null)
                return Unauthorized("Wrong login or password");

            return Ok(user);


        }

        [HttpPost("signup")]
        public async Task<IActionResult> Register([FromBody]RegistrationModel model)
        {
            if (model is null)
                return BadRequest("Registration data is empty");

            AuctionUserModel user = await _authorizationService.RegisterAsync(model);

            if (user is null)
                return Unauthorized("Wrong login or password");

            return Ok(user);
        }
    }
}

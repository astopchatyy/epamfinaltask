﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("rates")]
    [ApiController]
    public class BetController : ControllerBase
    {
        private readonly IBetService _betService;

        public BetController(IBetService betService)
        {
            _betService = betService;
        }

        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> PlaceBet([FromBody] PlaceBetModel model)
        {
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (model is null)
                return BadRequest("Creation data is empty");
            await _betService.PlaceBetAsync(requestUserName, model);

            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RemoveBet(int betId)
        {
            await _betService.RemoveBetByIdAsync(betId);
            return Ok();
        }
    }
}

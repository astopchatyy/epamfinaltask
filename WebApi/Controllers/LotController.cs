﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("")]
    public class LotController : ControllerBase
    {
        private readonly ILotService _lotService;
        private readonly ILotListeningService _lotListeningService;

        public LotController(ILotService lotService, ILotListeningService lotListeningService)
        {
            _lotService = lotService;
            _lotListeningService = lotListeningService;
        }

        [HttpGet]
        [Route("lots")]
        public async Task<IActionResult> GetLotList(string searchString)
        {

            IEnumerable<LotForListModel> lots = await _lotService.GetLotModelListAsync(
                new LotSearchModel { OwnerUserName = searchString, Title = searchString });

            List<Claim> identity = HttpContext.User.Claims.ToList();
            if (identity.Count > 0)
            {
                string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
                List<string> roles = identity.FindAll(claim => claim.Type == ClaimTypes.Role).Select(claim => claim.Value).ToList();
                if (roles.Contains("user"))
                {
                    await _lotListeningService.AllowListening(requestUserName,lots);
                }
            }

            return Ok(lots);
        }

        [HttpGet]
        [Route("lots/{id}")]
        [Authorize(Roles  = "user")]
        public async Task<IActionResult> GetLotDetails(int id) 
        {
            LotDetailsModel lot = await _lotService.GetLotDetailsAsync(id);
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            List<string> roles = identity.FindAll(claim => claim.Type == ClaimTypes.Role).Select(claim => claim.Value).ToList();
            if (roles.Contains("user"))
            {
                await _lotListeningService.AllowListening(requestUserName, lot);
            }

            return Ok(lot);
        }
        [HttpGet]
        [Route("{userName}/lots")]
        [Authorize]
        public async Task<IActionResult> GetUserLotList(string userName, string searchString)
        {
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            List<string> roles = identity.FindAll(claim => claim.Type == ClaimTypes.Role).Select(claim => claim.Value).ToList();
            if (userName != requestUserName && !roles.Contains("admin"))
            {
                return Forbid();
            }
            return Ok(await _lotService.GetLotListByUserAsync(userName, new LotSearchModel {Title = searchString }));
        }
        [HttpGet]
        [Route("{userName}/listened-lots")]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> GetUserListenedLotList(string userName, string searchString)
        {
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (userName != requestUserName)
            {
                return Forbid();
            }
            return Ok(await _lotService.GetListenedLotListByUserAsync(userName, new LotSearchModel { OwnerUserName = searchString, Title = searchString }));
        }

        [HttpPost]
        [Route("lots")]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> CreateLot([FromBody] LotCreateModel model)
        {
            if (model is null)
                return BadRequest("Creation model is empty");
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (model.UserName != requestUserName)
            {
                return Forbid();
            }
            await _lotService.CreateLotAsync(model);

            return Ok();
        }

        [HttpDelete]
        [Route("lots/{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> RemoveLot(int id)
        {
            await _lotService.DeleteLotByIdAsync(id);
            return Ok();
        }
    }
}

﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("payments")]
    [ApiController]
    [Authorize(Roles = "user")]
    public class AccountManagmentController : ControllerBase
    {
        private readonly IAccountManagmentService _accountManagment;
        public AccountManagmentController(IAccountManagmentService accountManagment)
        {
            _accountManagment = accountManagment;
        }

        [HttpPut]
        [Route("pay")]
        public async Task<IActionResult> Pay([FromBody] PaymentModel payment)
        {
            if (payment is null)
                return BadRequest("Payment is empty");
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (payment.UserName != requestUserName)
            {
                return Forbid();
            }

            await _accountManagment.PayAsync(payment);

            return Ok();
        }

        [HttpPut]
        [Route("withdraw")]
        public async Task<IActionResult> Withdraw([FromBody] PaymentModel payment)
        {
            if (payment is null)
                return BadRequest("Payment is empty");
            List<Claim> identity = HttpContext.User.Claims.ToList();
            string requestUserName = identity.FirstOrDefault(claim => claim.Type == ClaimTypes.Name).Value;
            if (payment.UserName != requestUserName)
            {
                return Forbid();
            }

            await _accountManagment.WithdrawAsync(payment);

            return Ok();
        }
    }
}

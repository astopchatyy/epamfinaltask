using AutoMapper;
using BusinessLogic;
using BusinessLogic.Interfaces;
using BusinessLogic.JwtElements;
using BusinessLogic.Models;
using BusinessLogic.Services;
using BusinessLogic.ValidationServices;
using DataAccess;
using DataAccess.Entities;
using DataAccess.Interfaces;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using Quartz;
using System;
using System.Linq;
using WebApi.Filters;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AuctionDBContext>(options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("AuctionDB")));

            services.AddIdentity<AuctionUser, IdentityRole>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AuctionDBContext>();

            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<ILotRepository, LotRepository>();
            services.AddScoped<IListeningRepository, ListeningRepository>();
            services.AddScoped<IHistoryRepository, HistoryRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<ILotService, LotService>();
            services.AddScoped<IBetService, BetService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<INotifyService, NotifyService>();
            services.AddScoped<IAccountManagmentService, AccountManagmentService>();
            services.AddScoped<IBetMessageBuilder, BetMessageBuilder >();
            services.AddScoped<IClosingMessageBuilder, ClosingMessageBuilder>();
            services.AddScoped<ILotClosingService, LotClosingService>();
            services.AddScoped<ILotListeningService, LotListeningService>();

            services.AddScoped<IJwtStringBuilder, JwtStringBuilder>();

            services.AddScoped<IValidationService<RegistrationModel>, RegistrationValidationService>();
            services.AddScoped<IValidationService<LotCreateModel>, LotValidationService>();
            services.AddScoped<IValidationService<MessageModel>, MessageValidationService>();

            services.AddScoped<IMapper>(mapper => new Mapper(new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfile()))));

            JwtSetting jwtSetting = new JwtSetting();

                    services.AddAuthorization()
                    .AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = jwtSetting.Issuer,

                            ValidateAudience = true,
                            ValidAudience = jwtSetting.Audience,

                            ValidateLifetime = true,

                            IssuerSigningKey = jwtSetting.SymmetricSecurityKey,
                            ValidateIssuerSigningKey = true,
                        };
                    });


            services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSwaggerDocument(config =>
            {
                config.DocumentName = "Auction test page";
                config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });



            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(ExceptionFilter));
            });
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseOpenApi();
                app.UseSwaggerUi3();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                
                app.UseHsts();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();

            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "authorization",
                    pattern: "{action}",
                    defaults: new { controller = "Authorization" });
                endpoints.MapControllerRoute(
                    name: "lots",
                    pattern: "{controller=Lot}/{username}/{action}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.StartupTimeout = new System.TimeSpan(0, 15, 0);

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
            
        }
    }
}

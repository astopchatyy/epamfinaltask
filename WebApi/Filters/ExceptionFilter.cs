﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace WebApi.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public ExceptionFilter()
        {
        }


        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            if(exception is ArgumentException)
            {
                if(exception.Message == "Unauthorized")
                {
                    context.Result = new UnauthorizedResult();
                }
                else 
                {
                    context.Result = new BadRequestObjectResult(exception.Message);
                }
            
                context.ExceptionHandled = true;
            }
        }
    }
}

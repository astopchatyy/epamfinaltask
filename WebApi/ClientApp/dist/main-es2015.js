(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\r\n  <app-nav-menu></app-nav-menu>\r\n  <div class=\"container\">\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n</body>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/error/error.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/error/error.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>error works!</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>Hello, world!</h1>\r\n<p>Welcome to your new single-page application, built with:</p>\r\n<ul>\r\n  <li><a href='https://get.asp.net/'>ASP.NET Core</a> and <a href='https://msdn.microsoft.com/en-us/library/67ef8sbd.aspx'>C#</a> for cross-platform server-side code</li>\r\n  <li><a href='https://angular.io/'>Angular</a> and <a href='http://www.typescriptlang.org/'>TypeScript</a> for client-side code</li>\r\n  <li><a href='http://getbootstrap.com/'>Bootstrap</a> for layout and styling</li>\r\n</ul>\r\n<p>To help you get started, we've also set up:</p>\r\n<ul>\r\n  <li><strong>Client-side navigation</strong>. For example, click <em>Counter</em> then <em>Back</em> to return here.</li>\r\n  <li><strong>Angular CLI integration</strong>. In development mode, there's no need to run <code>ng serve</code>. It runs in the background automatically, so your client-side resources are dynamically built on demand and the page refreshes when you modify any file.</li>\r\n  <li><strong>Efficient production builds</strong>. In production mode, development-time features are disabled, and your <code>dotnet publish</code> configuration automatically invokes <code>ng build</code> to produce minified, ahead-of-time compiled JavaScript files.</li>\r\n</ul>\r\n<p>The <code>ClientApp</code> subdirectory is a standard Angular CLI application. If you open a command prompt in that directory, you can run any <code>ng</code> command (e.g., <code>ng test</code>), or use <code>npm</code> to install extra packages into it.</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-control/list-control.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/list-control/list-control.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<input [(ngModel)] =\"searchString\" type=\"text\" />\n<button (click)=\"onSearch()\">Search</button>\n<br/>\n\n\n<ng-container *ngIf=\"!lotList\">\n    Loading data...\n</ng-container>\n\n<ng-container *ngIf=\"lotList\">\n    <app-lot-list [lotList] = \"lotList\" [url] =\"_router.url\"></app-lot-list>\n</ng-container>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/log-in/log-in.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/log-in/log-in.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ng-container *ngIf=\"error\">\n    <div class=\"error-text\">\n        {{ error }}\n    </div>\n</ng-container>\n\n<input [(ngModel)]=\"userName\" type=\"text\" placeholder=\"User Name\" />\n<br />\n<input [(ngModel)]=\"password\" type=\"password\" placeholder=\"Password\"/>\n<br />\n<button (click)=\"onSignIn()\">Sign in</button>\n<br />\n<br />\n<a [routerLink]=\"['/signup']\">sign up</a>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/log-out/log-out.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/log-out/log-out.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lot-details/lot-details.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lot-details/lot-details.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ng-container *ngIf=\"lot\">\r\n    <div class=\"lot-header\">{{ lot.title }}</div>\r\n    <div class=\"lot-body\">\r\n        <div class=\"lot-description\">\r\n            {{lot.description}}\r\n        </div>\r\n        <div class=\"lot-data\">\r\n            <div>Owner: {{lot.ownerUserName}}</div>\r\n            <div>Current Price: {{lot.currentPrice}}</div>\r\n            <div>Minimal next price: {{lot.minNextPrice}}</div>\r\n            <div>Closed at: {{getTime(lot.closingTime)}}  {{getDate(lot.closingTime)}} </div>\r\n        </div>\r\n        <div></div>\r\n    </div>\r\n    <div class=\"lot-footer\">\r\n        <ng-container *ngIf=\"lot.listeningAllowed\">\r\n            <button (click)=\"onSubscribe(lot)\">Listen</button>\r\n        </ng-container>\r\n    </div>\r\n    <br />\r\n    <div class = \"error-text\"  *ngIf=\"error\">\r\n        {{error}}\r\n    </div>\r\n    <br />\r\n    <form *ngIf=\"lot.ownerUserName!==userName\">\r\n        <input type=\"number\" [(ngModel)]=\"newPrice\" name =\"newPrice\"\r\n               min=\"{{lot.minNextPrice}}\" />\r\n        <button type=\"button\" (click)=\"onBet()\">I want it!</button>\r\n    </form>\r\n\r\n    <table *ngIf=\"history\">\r\n        <thead>\r\n            <tr>\r\n                <td>\r\n                   Challenger\r\n                </td>\r\n                <td>\r\n                    Price\r\n                </td>\r\n                <td>\r\n                    Time\r\n                </td>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let record of history\">\r\n                <td>\r\n                    {{record.userName}}\r\n                </td>\r\n                <td>\r\n                    {{record.price}}\r\n                </td>\r\n                <td>\r\n                    {{this.getDate(record.betTime)}}\r\n                    <br />\r\n                    {{this.getTime(record.betTime)}}\r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</ng-container>\r\n<ng-container *ngIf=\"!lot\">\r\n    Loading...\r\n</ng-container>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lot-list/lot-list.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lot-list/lot-list.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 id=\"tableLabel\">Lots</h1>\r\n\r\n<p *ngIf=\"!lotList\"><em>Loading...</em></p>\r\n\r\n<table class='table table-striped' aria-labelledby=\"tableLabel\" *ngIf=\"lotList\">\r\n    <tbody>\r\n        <ng-template [ngIf]=\"authorized \">\r\n            <tr *ngFor=\"let lot of lotList\" [routerLink]=\"['/details/', lot.lotId]\">\r\n                <td>\r\n                    <div class=\"lot-header\">{{ lot.title }}</div>\r\n                    <div class=\"lot-body\">\r\n                        <div class=\"lot-description\">\r\n                            {{lot.description}}\r\n                        </div>\r\n                        <div class=\"lot-data\">\r\n                            <div>Owner: {{lot.ownerUserName}}</div>\r\n                            <div>Current Price: {{lot.currentPrice}}</div>\r\n                            <div>Minimal next price: {{lot.minNextPrice}}</div>\r\n                            <div>Closed at: {{getTime(lot.closingTime)}}</div> \r\n                            <div>{{getDate(lot.closingTime)}}</div> \r\n                        </div>\r\n                        <div></div>\r\n                    </div>\r\n                    <div class=\"lot-footer\">\r\n                        <ng-container *ngIf=\"lot.listeningAllowed\">\r\n                            <button (click)=\"onSubscribe(lot)\">Listen</button>\r\n                        </ng-container>\r\n\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </ng-template>\r\n        <ng-template [ngIf]=\"!authorized \">\r\n            <tr *ngFor=\"let lot of lotList\">\r\n                <td>\r\n                    <div class=\"lot-header\">{{ lot.title }}</div>\r\n                    <div class=\"lot-body\">\r\n                        <div class=\"lot-description\">\r\n                            {{lot.description}}\r\n                        </div>\r\n                        <div class=\"lot-data\">\r\n                            <div>Owner: {{lot.ownerUserName}}</div>\r\n                            <div>Current Price: {{lot.currentPrice}}</div>\r\n                            <div>Minimal next price: {{lot.minNextPrice}}</div>\r\n                            <div>Closed at: {{getTime(lot.closingTime)}}</div>\r\n                            <div>{{getDate(lot.closingTime)}}</div>\r\n                        </div>\r\n                        <div></div>\r\n                    </div>\r\n                    <div class=\"lot-footer\">\r\n                        <ng-container *ngIf=\"lot.listeningAllowed\">\r\n                            <button (click)=\"onSubscribe(lot)\">Listen</button>\r\n                        </ng-container>\r\n\r\n                    </div>\r\n                </td>\r\n            </tr>\r\n        </ng-template>\r\n    </tbody>\r\n</table>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/message-list/message-list.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/message-list/message-list.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"dropdown show\" >\r\n    <div class=\"dot indicator\" *ngIf=\"hasUnread\"></div>\r\n    <div class=\"nav-link dropdown-toggle\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n        Messages\r\n    </div>\r\n\r\n    <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownMenuLink\" (click)=\"openDropdown($event)\">\r\n        <div class=\"message-container\" *ngIf=\"messageList\" >\r\n            <table>\r\n                <tbody>\r\n                    <tr *ngFor=\"let message of messageList\" (click) =\"onSelect(message)\">\r\n                        <td>\r\n                            {{this.getDate(message.time)}}\r\n                            <br />\r\n                            {{this.getTime(message.time)}}\r\n                        </td>\r\n                        <td>\r\n                            {{message.title}}\r\n                        </td>\r\n                        <td *ngIf=\"!message.isRead\">\r\n                            <div class=\"dot\"></div>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n        <div *ngIf=\"!hasMessages\">\r\n            Loading...\r\n        </div>\r\n        <div *ngIf=\"hasMessages && !messageList\">\r\n            No messages\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav-menu/nav-menu.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nav-menu/nav-menu.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<header>\r\n  <nav\r\n    class=\"navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3\"\r\n  >\r\n    <div class=\"container\">\r\n      <a class=\"navbar-brand\" [routerLink]=\"['/']\">Auction</a>\r\n      <button\r\n        class=\"navbar-toggler\"\r\n        type=\"button\"\r\n        data-toggle=\"collapse\"\r\n        data-target=\".navbar-collapse\"\r\n        aria-label=\"Toggle navigation\"\r\n        [attr.aria-expanded]=\"isExpanded\"\r\n        (click)=\"toggle()\"\r\n      >\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n      </button>\r\n      <div\r\n        class=\"navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse\"\r\n        [ngClass]=\"{ show: isExpanded }\"\r\n      >\r\n          <ul class=\"navbar-nav flex-grow\">\r\n              <li class=\"nav-item\"\r\n                  [routerLinkActive]=\"['link-active']\"\r\n                  [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                  <a class=\"nav-link text-dark\" [routerLink]=\"['/lot-list']\">Lots</a>\r\n              </li>\r\n              <ng-container *ngIf=\"authorizedAsUser\">\r\n                  <li class=\"nav-item\"\r\n                      [routerLinkActive]=\"['link-active']\"\r\n                      [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                      <a class=\"nav-link text-dark\" [routerLink]=\"['/user-lots']\">My lots</a>\r\n                  </li>\r\n                  <li class=\"nav-item\"\r\n                      [routerLinkActive]=\"['link-active']\"\r\n                      [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                      <a class=\"text-dark nav-link\" [routerLink]=\"['/listened-lots']\">Listened lots</a>\r\n                  </li>\r\n              </ng-container>\r\n              <ng-container *ngIf=\"authorized\">\r\n                  <li class=\"nav-item\">\r\n                      <app-message-list></app-message-list>\r\n                  </li>\r\n                  <li class=\"nav-item\"\r\n                      [routerLinkActive]=\"['link-active']\"\r\n                      [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                      <a class=\"nav-link text-dark\" [routerLink]=\"['/signout']\">Sign out</a>\r\n                  </li>\r\n              </ng-container>\r\n              <ng-container *ngIf=\"!authorized\">\r\n                  <li class=\"nav-item\"\r\n                      [routerLinkActive]=\"['link-active']\"\r\n                      [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                      <a class=\"nav-link text-dark\" [routerLink]=\"['/signin']\">Sign in</a>\r\n                  </li>\r\n                  <li class=\"nav-item\"\r\n                      [routerLinkActive]=\"['link-active']\"\r\n                      [routerLinkActiveOptions]=\"{ exact: true }\">\r\n                      <a class=\"nav-link text-dark\" [routerLink]=\"['/signup']\">Sign up</a>\r\n                  </li>\r\n              </ng-container>\r\n          </ul>\r\n      </div>\r\n    </div>\r\n  </nav>\r\n</header>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ng-container *ngIf=\"error\">\n    <div class=\"error-text\">\n        {{ error }}\n    </div>\n</ng-container>\n\n<input [(ngModel)]=\"userName\" type=\"text\" placeholder=\"User Name\"/>\n<br />\n<input [(ngModel)]=\"password\" type=\"password\" placeholder=\"Password\"/>\n<br />\n<input [(ngModel)]=\"passwordConfirmation\" type=\"password\" placeholder=\"Password Confirmation\"/>\n<br />\n<input [(ngModel)]=\"email\" type=\"email\" placeholder=\"Email\"/>\n<br />\n<button (click)=\"onSignUp()\">Sign up</button>\n<br />\n<br />\n<a [routerLink]=\"['/signin']\">sign in</a>\n\n");

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let AppComponent = class AppComponent {
    constructor() {
        this.title = 'app';
    }
};
AppComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-root',
        template: __importDefault(__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _nav_menu_nav_menu_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nav-menu/nav-menu.component */ "./src/app/nav-menu/nav-menu.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _lot_list_lot_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./lot-list/lot-list.component */ "./src/app/lot-list/lot-list.component.ts");
/* harmony import */ var _list_control_list_control_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./list-control/list-control.component */ "./src/app/list-control/list-control.component.ts");
/* harmony import */ var _log_in_log_in_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./log-in/log-in.component */ "./src/app/log-in/log-in.component.ts");
/* harmony import */ var _registration_registration_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./registration/registration.component */ "./src/app/registration/registration.component.ts");
/* harmony import */ var _lot_details_lot_details_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./lot-details/lot-details.component */ "./src/app/lot-details/lot-details.component.ts");
/* harmony import */ var _message_list_message_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./message-list/message-list.component */ "./src/app/message-list/message-list.component.ts");
/* harmony import */ var _error_error_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./error/error.component */ "./src/app/error/error.component.ts");
/* harmony import */ var _log_out_log_out_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./log-out/log-out.component */ "./src/app/log-out/log-out.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
















let AppModule = class AppModule {
};
AppModule = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _nav_menu_nav_menu_component__WEBPACK_IMPORTED_MODULE_6__["NavMenuComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
            _lot_list_lot_list_component__WEBPACK_IMPORTED_MODULE_8__["LotListComponent"],
            _list_control_list_control_component__WEBPACK_IMPORTED_MODULE_9__["ListControlComponent"],
            _log_in_log_in_component__WEBPACK_IMPORTED_MODULE_10__["LogInComponent"],
            _registration_registration_component__WEBPACK_IMPORTED_MODULE_11__["RegistrationComponent"],
            _lot_details_lot_details_component__WEBPACK_IMPORTED_MODULE_12__["LotDetailsComponent"],
            _message_list_message_list_component__WEBPACK_IMPORTED_MODULE_13__["MessageListComponent"],
            _error_error_component__WEBPACK_IMPORTED_MODULE_14__["ErrorComponent"],
            _log_out_log_out_component__WEBPACK_IMPORTED_MODULE_15__["LogOutComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"].withServerTransition({ appId: 'ng-cli-universal' }),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot([
                { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"], pathMatch: 'full' },
                { path: 'lot-list', component: _list_control_list_control_component__WEBPACK_IMPORTED_MODULE_9__["ListControlComponent"] },
                { path: 'user-lots', component: _list_control_list_control_component__WEBPACK_IMPORTED_MODULE_9__["ListControlComponent"] },
                { path: 'listened-lots', component: _list_control_list_control_component__WEBPACK_IMPORTED_MODULE_9__["ListControlComponent"] },
                { path: 'details/:id', component: _lot_details_lot_details_component__WEBPACK_IMPORTED_MODULE_12__["LotDetailsComponent"] },
                { path: 'signin', component: _log_in_log_in_component__WEBPACK_IMPORTED_MODULE_10__["LogInComponent"] },
                { path: 'signout', component: _log_out_log_out_component__WEBPACK_IMPORTED_MODULE_15__["LogOutComponent"] },
                { path: 'signup', component: _registration_registration_component__WEBPACK_IMPORTED_MODULE_11__["RegistrationComponent"] }
            ], { onSameUrlNavigation: 'reload' }),
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/error/error.component.css":
/*!*******************************************!*\
  !*** ./src/app/error/error.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Vycm9yL2Vycm9yLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/error/error.component.ts":
/*!******************************************!*\
  !*** ./src/app/error/error.component.ts ***!
  \******************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let ErrorComponent = class ErrorComponent {
    constructor() { }
    ngOnInit() {
    }
};
ErrorComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-error',
        template: __importDefault(__webpack_require__(/*! raw-loader!./error.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/error/error.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./error.component.css */ "./src/app/error/error.component.css")).default]
    }),
    __metadata("design:paramtypes", [])
], ErrorComponent);



/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};

let HomeComponent = class HomeComponent {
};
HomeComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-home',
        template: __importDefault(__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.component.html")).default,
    })
], HomeComponent);



/***/ }),

/***/ "./src/app/list-control/list-control.component.css":
/*!*********************************************************!*\
  !*** ./src/app/list-control/list-control.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xpc3QtY29udHJvbC9saXN0LWNvbnRyb2wuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/list-control/list-control.component.ts":
/*!********************************************************!*\
  !*** ./src/app/list-control/list-control.component.ts ***!
  \********************************************************/
/*! exports provided: ListControlComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListControlComponent", function() { return ListControlComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_lot_list_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/lot-list.service */ "./src/services/lot-list.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





let ListControlComponent = class ListControlComponent {
    constructor(_lotListService, _router) {
        this._lotListService = _lotListService;
        this._router = _router;
    }
    ngOnInit() {
        this.subscribeForLots();
    }
    subscribeForLots(searchString) {
        var observable;
        if (this._router.url === "/user-lots") {
            observable = this._lotListService.getLotsForUserBySearch(searchString);
        }
        else if (this._router.url === "/listened-lots") {
            observable = this._lotListService.getLisenedLotsForUserBySearch(searchString);
        }
        else {
            observable = this._lotListService.getLotsBySearch(searchString);
        }
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["timer"])(0, 10000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(() => observable))
            .subscribe(lots => this.lotList = lots, error => { this.error = error.error; console.log(error); });
    }
    onSearch() {
        this.subscription.unsubscribe();
        this.subscribeForLots(this.searchString);
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
};
ListControlComponent.ctorParameters = () => [
    { type: _services_lot_list_service__WEBPACK_IMPORTED_MODULE_4__["LotListService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
ListControlComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-list-control',
        template: __importDefault(__webpack_require__(/*! raw-loader!./list-control.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/list-control/list-control.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./list-control.component.css */ "./src/app/list-control/list-control.component.css")).default]
    }),
    __metadata("design:paramtypes", [_services_lot_list_service__WEBPACK_IMPORTED_MODULE_4__["LotListService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], ListControlComponent);



/***/ }),

/***/ "./src/app/log-in/log-in.component.css":
/*!*********************************************!*\
  !*** ./src/app/log-in/log-in.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error-text{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9nLWluL2xvZy1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksU0FBUztBQUNiIiwiZmlsZSI6InNyYy9hcHAvbG9nLWluL2xvZy1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yLXRleHR7XHJcbiAgICBjb2xvcjpyZWQ7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/log-in/log-in.component.ts":
/*!********************************************!*\
  !*** ./src/app/log-in/log-in.component.ts ***!
  \********************************************/
/*! exports provided: LogInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogInComponent", function() { return LogInComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/authorization.service */ "./src/services/authorization.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let LogInComponent = class LogInComponent {
    constructor(_router, _authorizationSevice) {
        this._router = _router;
        this._authorizationSevice = _authorizationSevice;
    }
    ngOnInit() {
        if (sessionStorage.getItem("token")) {
            this._router.navigateByUrl("/");
            return;
        }
    }
    onSignIn() {
        var logIn = { userName: this.userName, password: this.password };
        this._authorizationSevice.logIn(logIn).subscribe(user => {
            this.user = user;
            if (this.user) {
                sessionStorage.setItem("token", this.user.token);
                sessionStorage.setItem("userName", this.user.userName);
                sessionStorage.setItem("currentBalance", this.user.currentBalance.toString());
                sessionStorage.setItem("totalBalance", this.user.totalBalance.toString());
                if (this.user.roles.includes("user"))
                    sessionStorage.setItem("asUser", "true");
                if (this.user.roles.includes("admin"))
                    sessionStorage.setItem("asAdmin", "true");
                this._router.navigateByUrl("/");
            }
        }, error => { this.error = error.error, console.log(error); });
    }
};
LogInComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__["AuthorizationService"] }
];
LogInComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-log-in',
        template: __importDefault(__webpack_require__(/*! raw-loader!./log-in.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/log-in/log-in.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./log-in.component.css */ "./src/app/log-in/log-in.component.css")).default]
    }),
    __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__["AuthorizationService"]])
], LogInComponent);



/***/ }),

/***/ "./src/app/log-out/log-out.component.css":
/*!***********************************************!*\
  !*** ./src/app/log-out/log-out.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZy1vdXQvbG9nLW91dC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/log-out/log-out.component.ts":
/*!**********************************************!*\
  !*** ./src/app/log-out/log-out.component.ts ***!
  \**********************************************/
/*! exports provided: LogOutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogOutComponent", function() { return LogOutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


let LogOutComponent = class LogOutComponent {
    constructor(_router) {
        this._router = _router;
    }
    ngOnInit() {
        sessionStorage.clear();
        this._router.navigateByUrl("/");
    }
};
LogOutComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
LogOutComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-log-out',
        template: __importDefault(__webpack_require__(/*! raw-loader!./log-out.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/log-out/log-out.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./log-out.component.css */ "./src/app/log-out/log-out.component.css")).default]
    }),
    __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], LogOutComponent);



/***/ }),

/***/ "./src/app/lot-details/lot-details.component.css":
/*!*******************************************************!*\
  !*** ./src/app/lot-details/lot-details.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".lot-body > div {\r\n    display: inline-block;\r\n}\r\n\r\n.lot-description {\r\n    vertical-align: top;\r\n    width: 50%;\r\n}\r\n\r\n.lot-header {\r\n    text-align: center;\r\n    font-weight: bold;\r\n}\r\n\r\ntr td{\r\n    border:black solid;\r\n}\r\n\r\n.error-text {\r\n    color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG90LWRldGFpbHMvbG90LWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFFQTtJQUNJLG1CQUFtQjtJQUNuQixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUNBO0lBQ0ksVUFBVTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvbG90LWRldGFpbHMvbG90LWRldGFpbHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb3QtYm9keSA+IGRpdiB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5sb3QtZGVzY3JpcHRpb24ge1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcbi5sb3QtaGVhZGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcblxyXG50ciB0ZHtcclxuICAgIGJvcmRlcjpibGFjayBzb2xpZDtcclxufVxyXG4uZXJyb3ItdGV4dCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/lot-details/lot-details.component.ts":
/*!******************************************************!*\
  !*** ./src/app/lot-details/lot-details.component.ts ***!
  \******************************************************/
/*! exports provided: LotDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LotDetailsComponent", function() { return LotDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_bet_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/bet.service */ "./src/services/bet.service.ts");
/* harmony import */ var _services_lot_list_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/lot-list.service */ "./src/services/lot-list.service.ts");
/* harmony import */ var _services_lot_listening_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/lot-listening.service */ "./src/services/lot-listening.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





let LotDetailsComponent = class LotDetailsComponent {
    constructor(_router, _activatedRoute, _betService, _lotService, _listeningService) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._betService = _betService;
        this._lotService = _lotService;
        this._listeningService = _listeningService;
        this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    }
    ngOnInit() {
        this.userName = sessionStorage.getItem("userName");
        this._subscription = this._activatedRoute.paramMap.subscribe(m => this._id = Number.parseInt(m.get("id")));
        this._subscription.add(this._lotService.getLotById(this._id).subscribe(lot => {
            this.lot = lot;
            if (lot.betHistory)
                this.history = lot.betHistory;
            this.newPrice = lot.minNextPrice;
        }, error => this.error = error.error));
    }
    onBet() {
        this._subscription.add(this._betService.placeBet({ lotId: this._id, newPrice: this.newPrice })
            .subscribe(o => { this._router.navigateByUrl("/details/" + this._id); }, error => { this.error.error = error; console.log(error); }));
    }
    getDate(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth() + 1) + "." + tDate.getFullYear();
        return res;
    }
    getTime(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }
    onSubscribe() {
        var listening = { lotId: this._id, userName: sessionStorage.getItem("userName") };
        this._subscription = this._subscription.add(this._listeningService.addListener(listening).subscribe(o => { }, error => { this.error.error = error; console.log(error); }));
        this._router.navigateByUrl("/details/" + this._id);
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
LotDetailsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] },
    { type: _services_bet_service__WEBPACK_IMPORTED_MODULE_2__["BetService"] },
    { type: _services_lot_list_service__WEBPACK_IMPORTED_MODULE_3__["LotListService"] },
    { type: _services_lot_listening_service__WEBPACK_IMPORTED_MODULE_4__["LotListeningService"] }
];
LotDetailsComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-lot-details',
        template: __importDefault(__webpack_require__(/*! raw-loader!./lot-details.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lot-details/lot-details.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./lot-details.component.css */ "./src/app/lot-details/lot-details.component.css")).default]
    }),
    __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
        _services_bet_service__WEBPACK_IMPORTED_MODULE_2__["BetService"], _services_lot_list_service__WEBPACK_IMPORTED_MODULE_3__["LotListService"],
        _services_lot_listening_service__WEBPACK_IMPORTED_MODULE_4__["LotListeningService"]])
], LotDetailsComponent);



/***/ }),

/***/ "./src/app/lot-list/lot-list.component.css":
/*!*************************************************!*\
  !*** ./src/app/lot-list/lot-list.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".lot-body>div{\r\n    display: inline-block;\r\n}\r\n.lot-description{\r\n    vertical-align:top;\r\n    width: 50%;\r\n}\r\n.lot-header{\r\n    text-align:center;\r\n    font-weight:bold;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG90LWxpc3QvbG90LWxpc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFVBQVU7QUFDZDtBQUNBO0lBQ0ksaUJBQWlCO0lBQ2pCLGdCQUFnQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2xvdC1saXN0L2xvdC1saXN0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG90LWJvZHk+ZGl2e1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5sb3QtZGVzY3JpcHRpb257XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjp0b3A7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59XHJcbi5sb3QtaGVhZGVye1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBmb250LXdlaWdodDpib2xkO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/lot-list/lot-list.component.ts":
/*!************************************************!*\
  !*** ./src/app/lot-list/lot-list.component.ts ***!
  \************************************************/
/*! exports provided: LotListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LotListComponent", function() { return LotListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_lot_listening_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/lot-listening.service */ "./src/services/lot-listening.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};





let LotListComponent = class LotListComponent {
    constructor(_listeningService, _router) {
        this._listeningService = _listeningService;
        this._router = _router;
        this._subscription = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subscription"]();
        this.authorized = false;
    }
    ngOnInit() {
        this.authorized = !Object(util__WEBPACK_IMPORTED_MODULE_4__["isNullOrUndefined"])(sessionStorage.getItem("token"));
    }
    onSubscribe(lot) {
        var listening = { lotId: lot.lotId, userName: sessionStorage.getItem("userName") };
        this._subscription = this._subscription.add(this._listeningService.addListener(listening).subscribe(error => { this.error = error.error; console.log(error); }));
        this._router.navigateByUrl(this.url);
    }
    getDate(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth() + 1) + "." + tDate.getFullYear();
        return res;
    }
    getTime(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }
    ngOnDestroy() {
        if (this._subscription)
            this._subscription.unsubscribe();
    }
};
LotListComponent.ctorParameters = () => [
    { type: _services_lot_listening_service__WEBPACK_IMPORTED_MODULE_2__["LotListeningService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", Array)
], LotListComponent.prototype, "lotList", void 0);
__decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
    __metadata("design:type", String)
], LotListComponent.prototype, "url", void 0);
LotListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-lot-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./lot-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lot-list/lot-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./lot-list.component.css */ "./src/app/lot-list/lot-list.component.css")).default]
    }),
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [_services_lot_listening_service__WEBPACK_IMPORTED_MODULE_2__["LotListeningService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], LotListComponent);



/***/ }),

/***/ "./src/app/message-list/message-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/message-list/message-list.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".message-container {\r\n    width: 300px !important;\r\n    max-height: 300px;\r\n    overflow-x:visible;\r\n    overflow-y:scroll;\r\n}\r\n\r\ndiv {\r\n}\r\n\r\n.dot {\r\n    height: 10px;\r\n    width: 10px;\r\n    background-color: orange;\r\n    border-radius: 50%;\r\n}\r\n\r\n.indicator {\r\n    height: 8px;\r\n    width: 8px;\r\n    position: absolute;\r\n    margin-top: 23px;\r\n    margin-left: 3px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVzc2FnZS1saXN0L21lc3NhZ2UtbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaUJBQWlCO0FBQ3JCOztBQUVBO0FBQ0E7O0FBRUE7SUFDSSxZQUFZO0lBQ1osV0FBVztJQUNYLHdCQUF3QjtJQUN4QixrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0FBQ3BCIiwiZmlsZSI6InNyYy9hcHAvbWVzc2FnZS1saXN0L21lc3NhZ2UtbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1lc3NhZ2UtY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG4gICAgbWF4LWhlaWdodDogMzAwcHg7XHJcbiAgICBvdmVyZmxvdy14OnZpc2libGU7XHJcbiAgICBvdmVyZmxvdy15OnNjcm9sbDtcclxufVxyXG5cclxuZGl2IHtcclxufVxyXG5cclxuLmRvdCB7XHJcbiAgICBoZWlnaHQ6IDEwcHg7XHJcbiAgICB3aWR0aDogMTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IG9yYW5nZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG4uaW5kaWNhdG9yIHtcclxuICAgIGhlaWdodDogOHB4O1xyXG4gICAgd2lkdGg6IDhweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG1hcmdpbi10b3A6IDIzcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogM3B4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/message-list/message-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/message-list/message-list.component.ts ***!
  \********************************************************/
/*! exports provided: MessageListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageListComponent", function() { return MessageListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_message_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/message.service */ "./src/services/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let MessageListComponent = class MessageListComponent {
    constructor(_messageService) {
        this._messageService = _messageService;
        this.hasMessages = false;
        this.hasUnread = false;
    }
    ngOnInit() {
        this._subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["timer"])(0, 10000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(() => this._messageService.getMessages())).subscribe(messages => {
            this.messageList = messages.sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
            this.hasMessages = true;
            this.hasUnread = this.messageList.filter(message => !message.isRead).length > 0;
        }, error => console.error(error));
    }
    getDate(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth() + 1) + "." + tDate.getFullYear();
        return res;
    }
    getTime(date) {
        var res;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }
    onSelect(message) {
        console.log(message);
        this._messageService.markAsRead(message.messageId).subscribe(any => { }, error => console.log(error));
        message.isRead = true;
        this.hasUnread = this.messageList.filter(message => !message.isRead).length > 0;
    }
    openDropdown(event) {
        event.stopPropagation();
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
};
MessageListComponent.ctorParameters = () => [
    { type: _services_message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"] }
];
MessageListComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-message-list',
        template: __importDefault(__webpack_require__(/*! raw-loader!./message-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/message-list/message-list.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./message-list.component.css */ "./src/app/message-list/message-list.component.css")).default]
    }),
    __metadata("design:paramtypes", [_services_message_service__WEBPACK_IMPORTED_MODULE_3__["MessageService"]])
], MessageListComponent);



/***/ }),

/***/ "./src/app/nav-menu/nav-menu.component.css":
/*!*************************************************!*\
  !*** ./src/app/nav-menu/nav-menu.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("a.navbar-brand {\r\n  white-space: normal;\r\n  text-align: center;\r\n  word-break: break-all;\r\n}\r\n\r\nhtml {\r\n  font-size: 14px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n  html {\r\n    font-size: 16px;\r\n  }\r\n}\r\n\r\n.box-shadow {\r\n  box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2LW1lbnUvbmF2LW1lbnUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFDQTtFQUNFO0lBQ0UsZUFBZTtFQUNqQjtBQUNGOztBQUVBO0VBQ0UsOENBQThDO0FBQ2hEIiwiZmlsZSI6InNyYy9hcHAvbmF2LW1lbnUvbmF2LW1lbnUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImEubmF2YmFyLWJyYW5kIHtcclxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbn1cclxuXHJcbmh0bWwge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICBodG1sIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICB9XHJcbn1cclxuXHJcbi5ib3gtc2hhZG93IHtcclxuICBib3gtc2hhZG93OiAwIC4yNXJlbSAuNzVyZW0gcmdiYSgwLCAwLCAwLCAuMDUpO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/nav-menu/nav-menu.component.ts":
/*!************************************************!*\
  !*** ./src/app/nav-menu/nav-menu.component.ts ***!
  \************************************************/
/*! exports provided: NavMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavMenuComponent", function() { return NavMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! util */ "./node_modules/util/util.js");
/* harmony import */ var util__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let NavMenuComponent = class NavMenuComponent {
    constructor(_router) {
        this._router = _router;
        this.isExpanded = false;
        this.authorizedAsUser = false;
        this.authorized = false;
    }
    ngOnInit() {
        this._subscription = this._router.events.subscribe(event => {
            if (event.constructor.name === "NavigationEnd") {
                this.authorizedAsUser = !Object(util__WEBPACK_IMPORTED_MODULE_2__["isNullOrUndefined"])(sessionStorage.getItem("asUser"));
                this.authorized = !Object(util__WEBPACK_IMPORTED_MODULE_2__["isNullOrUndefined"])(sessionStorage.getItem("token"));
            }
        }, error => { console.log(error); });
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
    collapse() {
        this.isExpanded = false;
    }
    toggle() {
        this.isExpanded = !this.isExpanded;
    }
};
NavMenuComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
];
NavMenuComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-nav-menu',
        template: __importDefault(__webpack_require__(/*! raw-loader!./nav-menu.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/nav-menu/nav-menu.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./nav-menu.component.css */ "./src/app/nav-menu/nav-menu.component.css")).default]
    }),
    __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
], NavMenuComponent);



/***/ }),

/***/ "./src/app/registration/registration.component.css":
/*!*********************************************************!*\
  !*** ./src/app/registration/registration.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error-text {\r\n    color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksVUFBVTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0cmF0aW9uL3JlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yLXRleHQge1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/registration/registration.component.ts":
/*!********************************************************!*\
  !*** ./src/app/registration/registration.component.ts ***!
  \********************************************************/
/*! exports provided: RegistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationComponent", function() { return RegistrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/authorization.service */ "./src/services/authorization.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};



let RegistrationComponent = class RegistrationComponent {
    constructor(_router, _authorizationSevice) {
        this._router = _router;
        this._authorizationSevice = _authorizationSevice;
    }
    ngOnInit() {
        if (sessionStorage.getItem("token")) {
            this._router.navigateByUrl("/");
            return;
        }
    }
    onSignUp() {
        var registration = {
            userName: this.userName,
            password: this.password,
            passwordConfirmation: this.passwordConfirmation,
            email: this.email,
        };
        this.subscribtion = this._authorizationSevice.register(registration)
            .subscribe(o => { }, error => { this.error.error = error; console.log(error); }, () => this._router.navigateByUrl("/"));
    }
    ngOnDestroy() {
        if (this.subscribtion)
            this.subscribtion.unsubscribe();
    }
};
RegistrationComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__["AuthorizationService"] }
];
RegistrationComponent = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
        selector: 'app-registration',
        template: __importDefault(__webpack_require__(/*! raw-loader!./registration.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.component.html")).default,
        styles: [__importDefault(__webpack_require__(/*! ./registration.component.css */ "./src/app/registration/registration.component.css")).default]
    }),
    __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _services_authorization_service__WEBPACK_IMPORTED_MODULE_2__["AuthorizationService"]])
], RegistrationComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
const environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! exports provided: getBaseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBaseUrl", function() { return getBaseUrl; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
const providers = [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] }
];
if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])(providers).bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "./src/services/authorization.service.ts":
/*!***********************************************!*\
  !*** ./src/services/authorization.service.ts ***!
  \***********************************************/
/*! exports provided: AuthorizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizationService", function() { return AuthorizationService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


let AuthorizationService = class AuthorizationService {
    constructor(_httpClient, baseUrl) {
        this._httpClient = _httpClient;
        this.baseUrl = baseUrl;
    }
    register(registration) {
        return this._httpClient.post(this.baseUrl + "signup", registration);
    }
    logIn(logIn) {
        return this._httpClient.post(this.baseUrl + "signin", logIn);
    }
};
AuthorizationService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['BASE_URL',] }] }
];
AuthorizationService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('BASE_URL')),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], String])
], AuthorizationService);



/***/ }),

/***/ "./src/services/bet.service.ts":
/*!*************************************!*\
  !*** ./src/services/bet.service.ts ***!
  \*************************************/
/*! exports provided: BetService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BetService", function() { return BetService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _options_build_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./options-build.service */ "./src/services/options-build.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let BetService = class BetService {
    constructor(_httpClient, baseUrl) {
        this._httpClient = _httpClient;
        this.baseUrl = baseUrl;
        this.optionBuilder = new _options_build_service__WEBPACK_IMPORTED_MODULE_2__["OptionsBuildService"]();
    }
    placeBet(bet) {
        return this._httpClient.post(this.baseUrl + "rates", bet, this.optionBuilder.buildSimple());
    }
    removeBet(id) {
        return this._httpClient.delete(this.baseUrl + "rates/" + id, this.optionBuilder.buildSimple());
    }
};
BetService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['BASE_URL',] }] }
];
BetService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('BASE_URL')),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], String])
], BetService);



/***/ }),

/***/ "./src/services/lot-list.service.ts":
/*!******************************************!*\
  !*** ./src/services/lot-list.service.ts ***!
  \******************************************/
/*! exports provided: LotListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LotListService", function() { return LotListService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _options_build_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./options-build.service */ "./src/services/options-build.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let LotListService = class LotListService {
    constructor(_httpClient, baseUrl) {
        this._httpClient = _httpClient;
        this.baseUrl = baseUrl;
        this.optionBuilder = new _options_build_service__WEBPACK_IMPORTED_MODULE_2__["OptionsBuildService"]();
    }
    getLotsBySearch(searchString) {
        return this._httpClient.get(this.baseUrl + "lots", this.optionBuilder.buildWithSearchString(searchString));
    }
    getLisenedLotsForUserBySearch(searchString) {
        return this._httpClient.get(this.baseUrl + sessionStorage.getItem("userName") + "/listened-lots", this.optionBuilder.buildWithSearchString(searchString));
    }
    getLotsForUserBySearch(searchString) {
        return this._httpClient.get(this.baseUrl + sessionStorage.getItem("userName") + "/lots", this.optionBuilder.buildWithSearchString(searchString));
    }
    getLotById(id) {
        return this._httpClient.get(this.baseUrl + "lots/" + id, this.optionBuilder.buildSimple());
    }
    createLot(lot) {
        return this._httpClient.post(this.baseUrl + "lots", lot, this.optionBuilder.buildSimple());
    }
    deleteLot(id) {
        return this._httpClient.delete(this.baseUrl + "lots/" + id, this.optionBuilder.buildSimple());
    }
};
LotListService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: ['BASE_URL',] }] }
];
LotListService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
        providedIn: 'root'
    }),
    __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('BASE_URL')),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], String])
], LotListService);



/***/ }),

/***/ "./src/services/lot-listening.service.ts":
/*!***********************************************!*\
  !*** ./src/services/lot-listening.service.ts ***!
  \***********************************************/
/*! exports provided: LotListeningService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LotListeningService", function() { return LotListeningService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _options_build_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./options-build.service */ "./src/services/options-build.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let LotListeningService = class LotListeningService {
    constructor(_httpClient, baseUrl) {
        this._httpClient = _httpClient;
        this.baseUrl = baseUrl;
        this.optionBuilder = new _options_build_service__WEBPACK_IMPORTED_MODULE_2__["OptionsBuildService"]();
    }
    addListener(lotListening) {
        return this._httpClient.post(this.baseUrl + "listenings", lotListening, this.optionBuilder.buildSimple());
    }
    removeListener(lotListening) {
        return this._httpClient.delete(this.baseUrl + "listenings", this.optionBuilder.buildWithBody(lotListening));
    }
};
LotListeningService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['BASE_URL',] }] }
];
LotListeningService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('BASE_URL')),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], String])
], LotListeningService);



/***/ }),

/***/ "./src/services/message.service.ts":
/*!*****************************************!*\
  !*** ./src/services/message.service.ts ***!
  \*****************************************/
/*! exports provided: MessageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageService", function() { return MessageService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _options_build_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./options-build.service */ "./src/services/options-build.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};




let MessageService = class MessageService {
    constructor(_httpClient, baseUrl) {
        this._httpClient = _httpClient;
        this.baseUrl = baseUrl;
        this.optionBuilder = new _options_build_service__WEBPACK_IMPORTED_MODULE_2__["OptionsBuildService"]();
    }
    getMessages() {
        return this._httpClient.get(this.baseUrl + "messages", this.optionBuilder.buildSimple());
    }
    getMessageById(id) {
        return this._httpClient.get(this.baseUrl + "messages/" + id, this.optionBuilder.buildSimple());
    }
    deleteMessageById(id) {
        return this._httpClient.delete(this.baseUrl + "messages/" + id, this.optionBuilder.buildSimple());
    }
    markAsRead(id) {
        return this._httpClient.put(this.baseUrl + "messages/" + id, {}, this.optionBuilder.buildSimple());
    }
};
MessageService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: ['BASE_URL',] }] }
];
MessageService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])('BASE_URL')),
    __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], String])
], MessageService);



/***/ }),

/***/ "./src/services/options-build.service.ts":
/*!***********************************************!*\
  !*** ./src/services/options-build.service.ts ***!
  \***********************************************/
/*! exports provided: OptionsBuildService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OptionsBuildService", function() { return OptionsBuildService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (undefined && undefined.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};


let OptionsBuildService = class OptionsBuildService {
    constructor() { }
    buildSimple() {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
        if (sessionStorage.getItem("token")) {
            headers = headers.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
        }
        return {
            headers: headers
        };
    }
    buildWithSearchString(searchString) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]();
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
        if (searchString) {
            params = params.set("searchString", searchString);
        }
        if (sessionStorage.getItem("token")) {
            headers = headers.set("Authorization", "Bearer " + sessionStorage.getItem("token"));
        }
        return {
            headers: headers,
            params: params
        };
    }
    buildWithBody(body) {
        var params;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
        if (body)
            params = {
                body: body,
            };
        if (sessionStorage.getItem("token"))
            headers = headers.set("Authorization", "Bearer " + sessionStorage.getItem("token"));
        return {
            headers: headers,
            params: params
        };
    }
};
OptionsBuildService = __decorate([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [])
], OptionsBuildService);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\astopchatyy\source\repos\Auction\WebApi\ClientApp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map
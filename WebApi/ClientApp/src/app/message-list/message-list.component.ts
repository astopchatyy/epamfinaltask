import { Component, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Message } from '../../models/message';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit {
    private _subscription: Subscription;
    messageList: Message[];
    messagesLoaded = false;
    hasMessages = false;
    hasUnread: boolean = false;

    constructor(private _messageService: MessageService) { }


    ngOnInit() {
           this._subscription = timer(0, 10000).pipe(
               switchMap(() => this._messageService.getMessages())
           ).subscribe(messages =>
           {
               this.messageList = messages.sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime());
               this.messagesLoaded = true;
               this.hasMessages = this.messageList.length > 0;
               this.hasUnread = this.messageList.filter(message => !message.isRead).length > 0;
           }, error => console.error(error));
    }

    public getDate(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth()+1) + "." + tDate.getFullYear();
        return res;
    }
    public getTime(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }

    onSelect(message: Message) {
        console.log(message);
        this._messageService.markAsRead(message.messageId).subscribe(any => { },error => console.log(error));
        message.isRead = true;
        this.hasUnread = this.messageList.filter(message => !message.isRead).length > 0;
    }

    openDropdown(event) {
        event.stopPropagation();
    }

    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
    /*

        */ 
}

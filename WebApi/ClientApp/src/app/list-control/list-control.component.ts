import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { LotPreview } from '../../models/lot-preview';
import { LotListService } from '../../services/lot-list.service';

@Component({
  selector: 'app-list-control',
  templateUrl: './list-control.component.html',
  styleUrls: ['./list-control.component.css']
})
export class ListControlComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    public lotList: LotPreview[];
    error;
    searchString: string;
    constructor(private _lotListService: LotListService, private _router: Router) { }

    ngOnInit() {
        this.subscribeForLots();
    }

    subscribeForLots(searchString?: string) {
        var observable:Observable<LotPreview[]>;
        if (this._router.url === "/user-lots") {
            observable = this._lotListService.getLotsForUserBySearch(searchString);
        }
        else if (this._router.url === "/listened-lots") {
            
            observable = this._lotListService.getLisenedLotsForUserBySearch(searchString);
        }
        else {
            observable = this._lotListService.getLotsBySearch(searchString);
                
        }
        this.subscription = timer(0, 10000).pipe(switchMap(() => observable))
            .subscribe(lots => this.lotList = lots, error => { this.error = error.error; console.log(error); });

    }

    onSearch() {
        this.subscription.unsubscribe();
        this.subscribeForLots(this.searchString);
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}

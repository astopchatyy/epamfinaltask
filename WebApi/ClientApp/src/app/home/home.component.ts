import { OnInit } from '@angular/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit{

    userName: string;

    ngOnInit() {
        this.userName = sessionStorage.getItem("userName");
    }
}

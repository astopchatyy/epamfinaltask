import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BetHistory } from '../../models/bet-history';
import { LotDetails } from '../../models/lot-details';
import { LotListening } from '../../models/lot-listening';
import { BetService } from '../../services/bet.service';
import { LotListService } from '../../services/lot-list.service';
import { LotListeningService } from '../../services/lot-listening.service';

@Component({
  selector: 'app-lot-details',
  templateUrl: './lot-details.component.html',
  styleUrls: ['./lot-details.component.css']
})
export class LotDetailsComponent implements OnInit, OnDestroy {

    public lot: LotDetails;
    public history: BetHistory[];
    newPrice: number;
    userName: string;
    error;
    private _id: number;
    private _subscription: Subscription;
    constructor(private _router: Router, private _activatedRoute: ActivatedRoute,
        private _betService: BetService, private _lotService: LotListService,
        private _listeningService: LotListeningService) {
        this._router.routeReuseStrategy.shouldReuseRoute = () => false
    }

    ngOnInit() {
        this.userName = sessionStorage.getItem("userName");
        this._subscription = this._activatedRoute.paramMap.subscribe(m => this._id = Number.parseInt(m.get("id")));
        this._subscription.add(this._lotService.getLotById(this._id).subscribe(lot => {
            this.lot = lot;
            if (lot.betHistory)
                this.history = lot.betHistory;
            this.newPrice = lot.minNextPrice;
        }, error => this.error = error.error));
    }

    onBet() {
        this._subscription.add(this._betService.placeBet({ lotId: this._id, newPrice: this.newPrice })
            .subscribe(o => { this._router.navigateByUrl("/details/" + this._id); },
                error => { this.error.error = error; console.log(error); }));
        
    }

    public getDate(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth() + 1) + "." + tDate.getFullYear();
        return res;
    }
    public getTime(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }

    onSubscribe() {
        var listening: LotListening = { lotId: this._id, userName: sessionStorage.getItem("userName") };
        this._subscription = this._subscription.add(
            this._listeningService.addListener(listening).subscribe(o => { }, error => { this.error.error = error; console.log(error); }));
        this._router.navigateByUrl("/details/" + this._id);
    }

    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { Registration } from '../../models/registration';
import { User } from '../../models/user';
import { AuthorizationService } from '../../services/authorization.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {

    constructor(private _router: Router, private _authorizationSevice: AuthorizationService) { }
    userName: string;
    password: string;
    passwordConfirmation: string;
    email: string;
    error;
    private subscribtion: Subscription;

    ngOnInit() {
        if (sessionStorage.getItem("token")) {
            this._router.navigateByUrl("/");
            return;
        }
    }

    onSignUp() {
        var registration: Registration =
        {
            userName: this.userName,
            password: this.password,
            passwordConfirmation: this.passwordConfirmation,
            email: this.email,
        };
        this.subscribtion = this._authorizationSevice.register(registration)
            .subscribe(o => { }, error => { this.error.error = error; console.log(error); }, () => this._router.navigateByUrl("/"));
        
    }
    ngOnDestroy() {
        if (this.subscribtion)
        this.subscribtion.unsubscribe();
    }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogIn } from '../../models/log-in';
import { User } from '../../models/user';
import { AuthorizationService } from '../../services/authorization.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

    constructor(private _router: Router, private _authorizationSevice:AuthorizationService) { }
    userName: string;
    password: string;
    error: string;
    user: User;

    ngOnInit() {
        if (sessionStorage.getItem("token")) {
            this._router.navigateByUrl("/");
            return;
        }
    }
    onSignIn() {
        var logIn: LogIn = { userName: this.userName, password: this.password };
        this._authorizationSevice.logIn(logIn).subscribe(user => {
            this.user = user;
            if (this.user) {
                sessionStorage.setItem("token", this.user.token);
                sessionStorage.setItem("userName", this.user.userName);
                sessionStorage.setItem("currentBalance", this.user.currentBalance.toString());
                sessionStorage.setItem("totalBalance", this.user.totalBalance.toString());
                if (this.user.roles.includes("user"))
                    sessionStorage.setItem("asUser", "true");
                if (this.user.roles.includes("admin"))
                    sessionStorage.setItem("asAdmin", "true");
                this._router.navigateByUrl("/");
            }
        }, error => { this.error = error.error, console.log(error) });

    }

}

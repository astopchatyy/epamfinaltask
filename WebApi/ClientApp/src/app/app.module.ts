import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LotListComponent } from './lot-list/lot-list.component';
import { ListControlComponent } from './list-control/list-control.component';
import { LogInComponent } from './log-in/log-in.component';
import { RegistrationComponent } from './registration/registration.component';
import { LotDetailsComponent } from './lot-details/lot-details.component';
import { MessageListComponent } from './message-list/message-list.component';
import { ErrorComponent } from './error/error.component';
import { LogOutComponent } from './log-out/log-out.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LotListComponent,
    ListControlComponent,
    LogInComponent,
    RegistrationComponent,
    LotDetailsComponent,
    MessageListComponent,
    ErrorComponent,
    LogOutComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'lot-list', component: ListControlComponent},
      { path: 'user-lots', component: ListControlComponent},
      { path: 'listened-lots', component: ListControlComponent},
        { path: 'details/:id', component: LotDetailsComponent },
        { path: 'signin', component: LogInComponent},
        { path: 'signout', component: LogOutComponent },
        { path: 'signup', component: RegistrationComponent }

    ], { onSameUrlNavigation: 'reload' }),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

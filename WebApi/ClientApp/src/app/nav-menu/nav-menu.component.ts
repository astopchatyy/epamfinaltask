import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit, OnDestroy {

    constructor(private _router: Router) { }

    isExpanded = false;
    authorizedAsUser: boolean = false;
    authorized: boolean = false;
    private _subscription: Subscription;

    ngOnInit(): void {
        this._subscription = this._router.events.subscribe(event => {
            if (event.constructor.name === "NavigationEnd") {
                this.authorizedAsUser = !isNullOrUndefined(sessionStorage.getItem("asUser"));
                this.authorized = !isNullOrUndefined(sessionStorage.getItem("token"));
            }
        },error => {console.log(error); })
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}

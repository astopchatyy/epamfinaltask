import { Component, OnInit, OnDestroy, Injectable, Input } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { LotPreview } from '../../models/lot-preview';
import { LotListeningService } from '../../services/lot-listening.service';
import { LotListening } from '../../models/lot-listening';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-lot-list',
  templateUrl: './lot-list.component.html',
  styleUrls: ['./lot-list.component.css']
})

@Injectable({
    providedIn: 'root'
})



export class LotListComponent implements OnInit, OnDestroy {
    private _subscription: Subscription = new Subscription();
    error;

    authorized: boolean = false;

    @Input()
    lotList: LotPreview[];
    @Input()
    url: string;
    constructor(private _listeningService: LotListeningService, private _router: Router) { }

    ngOnInit() {
        this.authorized = !isNullOrUndefined(sessionStorage.getItem("token"));
    }

    onSubscribe(lot: LotPreview) {
        var listening: LotListening = { lotId: lot.lotId, userName: sessionStorage.getItem("userName") };
        this._subscription = this._subscription.add(
            this._listeningService.addListener(listening).subscribe(error =>
            { this.error = error.error; console.log(error); }));
        this._router.navigateByUrl(this.url);
    }

    public getDate(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getDate() + "." + (tDate.getUTCMonth() + 1) + "." + tDate.getFullYear();
        return res;
    }
    public getTime(date: Date): string {
        var res: string;
        var tDate = new Date(date);
        res = tDate.getHours() + ":" + tDate.getMinutes() + ":" + tDate.getSeconds();
        return res;
    }

    ngOnDestroy() {
        if (this._subscription)
        this._subscription.unsubscribe();
    }
}

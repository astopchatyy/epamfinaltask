import { TestBed } from '@angular/core/testing';

import { OptionsBuildService } from './options-build.service';

describe('OptionsBuildService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OptionsBuildService = TestBed.get(OptionsBuildService);
    expect(service).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from '../models/message';
import { OptionsBuildService } from './options-build.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

    optionBuilder: OptionsBuildService;

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        this.optionBuilder = new OptionsBuildService();
    }

    getMessages(): Observable<Message[]> {
        return this._httpClient.get<Message[]>(this.baseUrl + "messages",
            this.optionBuilder.buildSimple());
    }
    getMessageById(id: number): Observable<Message> {
        return this._httpClient.get<Message>(this.baseUrl + "messages/" + id,
            this.optionBuilder.buildSimple());
    }
    deleteMessageById(id: number): Observable<any> {
        return this._httpClient.delete<any>(this.baseUrl + "messages/" + id,
            this.optionBuilder.buildSimple());

    }
    markAsRead(id: number): Observable<any> {
        return this._httpClient.put<any>(this.baseUrl + "messages/" + id, {},
            this.optionBuilder.buildSimple());
    }
}

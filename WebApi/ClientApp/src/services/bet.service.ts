import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bet } from '../models/bet';
import { OptionsBuildService } from './options-build.service';

@Injectable({
  providedIn: 'root'
})
export class BetService {

    optionBuilder: OptionsBuildService;

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        this.optionBuilder = new OptionsBuildService();
    }

    placeBet(bet: Bet): Observable<any> {
        return this._httpClient.post<any>(this.baseUrl + "rates", bet,
            this.optionBuilder.buildSimple());
    }
    removeBet(id: number): Observable<any> {
        return this._httpClient.delete<any>(this.baseUrl + "rates/" + id,
            this.optionBuilder.buildSimple());
    }
}

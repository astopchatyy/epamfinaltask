import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LotListening } from '../models/lot-listening';

@Injectable({
  providedIn: 'root'
})
export class OptionsBuildService {

    constructor() { }

    buildSimple(): object {
        var headers = new HttpHeaders();
        if (sessionStorage.getItem("token")) {
            headers = headers.append("Authorization", "Bearer " + sessionStorage.getItem("token"));
        }
        return {
            headers: headers
        }
    }

    buildWithSearchString(searchString: string):object {
        var params = new HttpParams();
        var headers = new HttpHeaders();
        if (searchString) {
            params=params.set("searchString", searchString);
        }
        if (sessionStorage.getItem("token")) {
            headers=headers.set("Authorization", "Bearer " + sessionStorage.getItem("token"));
        }
        return {
            headers: headers,
            params: params
        }
    }

    buildWithBody(body: object): object {
        var params;
        var headers = new HttpHeaders();
        if (body)
            params = {
                body: body,
            }
        if (sessionStorage.getItem("token"))
            headers =headers.set("Authorization", "Bearer " + sessionStorage.getItem("token"));

        return {
            headers: headers,
            params: params
        }
    }
}

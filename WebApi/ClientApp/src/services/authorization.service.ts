import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LogIn } from '../models/log-in';
import { Registration } from '../models/registration';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    }

    register(registration: Registration): Observable<any> {
        return this._httpClient.post<any>(this.baseUrl + "signup", registration);
    }

    logIn(logIn: LogIn): Observable<User> {
        return this._httpClient.post<User>(this.baseUrl + "signin", logIn);
    }
}

import { TestBed } from '@angular/core/testing';

import { LotListeningService } from './lot-listening.service';

describe('LotListeningService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LotListeningService = TestBed.get(LotListeningService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { AccountManagmentService } from './account-managment.service';

describe('AccountManagmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountManagmentService = TestBed.get(AccountManagmentService);
    expect(service).toBeTruthy();
  });
});

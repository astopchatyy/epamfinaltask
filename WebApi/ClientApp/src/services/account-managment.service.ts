import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Payment } from '../models/payment';
import { OptionsBuildService } from './options-build.service';

@Injectable({
  providedIn: 'root'
})
export class AccountManagmentService {

    optionBuilder: OptionsBuildService;

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        this.optionBuilder = new OptionsBuildService();
    }

    pay(payment: Payment): Observable<any> {
        return this._httpClient.put<any>(this.baseUrl + "payments/pay",
            this.optionBuilder.buildWithBody(payment));
    }

    withdraw(payment: Payment): Observable<any> {
        return this._httpClient.put<any>(this.baseUrl + "payments/withdraw",
            this.optionBuilder.buildWithBody(payment));
    }
}

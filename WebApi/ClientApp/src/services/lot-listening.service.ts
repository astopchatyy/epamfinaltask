import { HttpClient } from '@angular/common/http';
import { Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LotListening } from '../models/lot-listening';
import { OptionsBuildService } from './options-build.service';

@Injectable({
  providedIn: 'root'
})
export class LotListeningService {

    optionBuilder: OptionsBuildService;

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        this.optionBuilder = new OptionsBuildService();
    }

    addListener(lotListening: LotListening): Observable<any> {
        return this._httpClient.post<LotListening>(this.baseUrl + "listenings", lotListening,
            this.optionBuilder.buildSimple());
    }
    removeListener(lotListening: LotListening): Observable<any> {
        return this._httpClient.delete<any>(this.baseUrl + "listenings",
            this.optionBuilder.buildWithBody(lotListening));
    }
}

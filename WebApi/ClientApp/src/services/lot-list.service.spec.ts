import { TestBed } from '@angular/core/testing';

import { LotListService } from './lot-list.service';

describe('LotListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LotListService = TestBed.get(LotListService);
    expect(service).toBeTruthy();
  });
});

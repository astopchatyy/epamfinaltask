import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject } from '@angular/core';
import { LotPreview } from '../models/lot-preview';
import { Observable } from 'rxjs';
import { LotDetails } from '../models/lot-details';
import { OptionsBuildService } from './options-build.service';
import { LotCreate } from '../models/lot-create';


@Injectable({
  providedIn: 'root'
})
export class LotListService {

    optionBuilder: OptionsBuildService;

    constructor(private _httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
        this.optionBuilder = new OptionsBuildService();
    }

    getLotsBySearch(searchString?: string): Observable<LotPreview[]> {
        return this._httpClient.get<LotPreview[]>(this.baseUrl + "lots",
            this.optionBuilder.buildWithSearchString(searchString));

    }
    getLisenedLotsForUserBySearch(searchString?: string): Observable<LotPreview[]> {
        return this._httpClient.get<LotPreview[]>(this.baseUrl + sessionStorage.getItem("userName") + "/listened-lots",
            this.optionBuilder.buildWithSearchString(searchString));

    }
    getLotsForUserBySearch(searchString?: string): Observable<LotPreview[]> {
        return this._httpClient.get<LotPreview[]>(this.baseUrl + sessionStorage.getItem("userName") + "/lots",
            this.optionBuilder.buildWithSearchString(searchString));

    }
    getLotById(id: number): Observable<LotDetails> {
        return this._httpClient.get<LotDetails>(this.baseUrl + "lots/" + id, this.optionBuilder.buildSimple());

    }

    createLot(lot: LotCreate) : Observable<any>{
        return this._httpClient.post<any>(this.baseUrl + "lots", lot, this.optionBuilder.buildSimple());
    }
    deleteLot(id: number): Observable<any> {
        return this._httpClient.delete<any>(this.baseUrl + "lots/" + id, this.optionBuilder.buildSimple());
    }
}

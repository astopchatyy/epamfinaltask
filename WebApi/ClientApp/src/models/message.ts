export interface Message {
    messageId: number;
    title: string;
    text: string;
    time: Date;
    isRead: boolean;
}

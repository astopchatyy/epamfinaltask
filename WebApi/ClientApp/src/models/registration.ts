export interface Registration {
    userName: string;
    password: string;
    passwordConfirmation: string;
    email: string;
}

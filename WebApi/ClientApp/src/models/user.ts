export interface User {
    userName: string;
    token: string;
    currentBalance: number;
    totalBalance: number;
    roles: string[];
}

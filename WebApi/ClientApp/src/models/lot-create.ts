export interface LotCreate {
    userName: string;
    title: string;
    description: string;
    startPrice: number;
    priceStep: number
    maxPriceForProlong: number;
    closingTime: Date;
    prolongValue: number;
}

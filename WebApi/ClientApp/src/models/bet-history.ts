export interface BetHistory {
    id: number;
    userName: string;
    betTime: Date;
    price: number;
}

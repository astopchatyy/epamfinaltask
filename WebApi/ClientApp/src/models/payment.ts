export interface Payment {
    userName: string;
    amount: number;
}

import { BetHistory } from "./bet-history";

export interface LotDetails {
    lotId: number;
    title: string;
    description: string;
    closingTime: Date;
    prolongable: boolean;
    ownerUserName: string;
    listeningAllowed: boolean;
    isClosed: boolean;
    currentPrice: number;
    minNextPrice: number;
    betHistory: Array<BetHistory>;
}

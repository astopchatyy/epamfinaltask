export interface LotPreview {
    lotId: number;
    title: string;
    description: string;
    ownerUserName: string;
    closingTime: Date;
    currentPrice: number;
    minNextPrice: number;
    listeningAllowed: boolean;
}

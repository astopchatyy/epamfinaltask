﻿namespace DataAccess.Entities
{
    public enum MessageStat
    {
        New = 0,
        Read = 1
    }
}

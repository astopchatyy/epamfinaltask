﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Lot: BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }


        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual AuctionUser User { get; set; }


        public DateTime Created { get; set; }
        public DateTime ClosingTime { get; set; }
        public double ProlongValue { get; set; }

        public bool IsClosed { get; set; }


        public virtual ICollection<BetHistory> LotHistory { get; set; }
        public virtual ICollection<LotListening> Listeners { get; set; }


        [Column(TypeName = "decimal(18,4)")]
        public decimal CurrentPrice { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal PriceStep { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal MaxPriceForProlong { get; set; }
    }
}

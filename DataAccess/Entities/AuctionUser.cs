﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class AuctionUser : IdentityUser
    {
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<BetHistory> UserHistory { get; set; }
        public virtual ICollection<LotListening> ListenedLots { get; set; }


        [Column(TypeName = "decimal(18,4)")]
        public decimal TotalBalance { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal CurrentBalance { get; set; }
    }
}

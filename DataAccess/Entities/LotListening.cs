﻿namespace DataAccess.Entities
{
    public class LotListening : BaseEntity
    {
        public int LotId { get; set; }
        public virtual Lot Lot { get; set; }

        public string UserId { get; set; }
        public virtual AuctionUser User { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Message : BaseEntity
    {

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual AuctionUser User { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public int Stat { get; set; }

    }
}

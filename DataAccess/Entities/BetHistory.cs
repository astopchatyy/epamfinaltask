﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class BetHistory : BaseEntity
    {
        [Column(TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }

        public DateTime BetTime { get; set; }
        public int LotId { get; set; }
        public virtual Lot Lot { get; set; }

        public string UserId { get; set; }
        public virtual AuctionUser User { get; set; }
    }
}

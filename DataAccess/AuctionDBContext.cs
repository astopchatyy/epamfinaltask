﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class AuctionDBContext : IdentityDbContext<AuctionUser>
    {
        public DbSet<Lot> Lots { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<BetHistory> Histories { get; set; }
        public DbSet<LotListening> Listenings { get; set; }

        public AuctionDBContext(DbContextOptions<AuctionDBContext> options) : base(options)
        {
        }

        protected AuctionDBContext() : base()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}

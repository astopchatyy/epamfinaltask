﻿using DataAccess.Entities;

namespace DataAccess.Interfaces
{
    public interface ILotRepository : IRepository<Lot>
    {
    }
}

﻿using DataAccess.Entities;

namespace DataAccess.Interfaces
{
    public interface IListeningRepository : IRepository<LotListening>
    {
    }
}

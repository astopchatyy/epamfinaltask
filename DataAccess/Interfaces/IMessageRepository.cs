﻿using DataAccess.Entities;

namespace DataAccess.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
    }
}

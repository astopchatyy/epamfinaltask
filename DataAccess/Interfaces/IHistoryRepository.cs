﻿using DataAccess.Entities;

namespace DataAccess.Interfaces
{
    public interface IHistoryRepository : IRepository<BetHistory>
    {
    }
}

﻿using DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        public Task SaveAllChangesAsync();
        public void SaveAllChanges();
        public IMessageRepository MessageRepository { get; set; }
        public IListeningRepository ListeningRepository { get; set; }
        public ILotRepository LotRepository { get; set; }
        public IHistoryRepository HistoryRepository { get; set; }
        public UserManager<AuctionUser> UserManager { get; set; }
        public RoleManager<IdentityRole> RoleManager { get; set; }
    }
}

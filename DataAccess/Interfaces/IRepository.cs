﻿using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
    public interface IRepository<T> where T: BaseEntity
    {
        void Add(T entity);
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        void Update(T entity);
        void DeleteById(int id);
        void Delete(T entity);
        Task SaveChangesAsync();
        void SaveChanges();
    }
}

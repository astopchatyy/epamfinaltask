﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IMessageRepository messageRepository, IListeningRepository listeningRepository,
            ILotRepository lotRepository, IHistoryRepository historyRepository,
            UserManager<AuctionUser> users, RoleManager<IdentityRole> roles)
        {
            MessageRepository = messageRepository;
            ListeningRepository = listeningRepository;
            LotRepository = lotRepository;
            HistoryRepository = historyRepository;
            UserManager = users;
            RoleManager = roles;
        }
        public IMessageRepository MessageRepository { get; set; }
        public IListeningRepository ListeningRepository { get; set; }
        public ILotRepository LotRepository { get; set; }
        public IHistoryRepository HistoryRepository { get; set; }
        public UserManager<AuctionUser> UserManager { get; set; }
        public RoleManager<IdentityRole> RoleManager { get; set; }

        public void SaveAllChanges()
        {
            MessageRepository.SaveChanges();
            ListeningRepository.SaveChanges();
            LotRepository.SaveChanges();
            HistoryRepository.SaveChanges();
        }

        public async Task SaveAllChangesAsync()
        {
            await MessageRepository.SaveChangesAsync();
            await ListeningRepository.SaveChangesAsync();
            await LotRepository.SaveChangesAsync();
            await HistoryRepository.SaveChangesAsync();
        }
    }
}

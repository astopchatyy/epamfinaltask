﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class ListeningRepository : IListeningRepository
    {
        private readonly AuctionDBContext _context;
        public ListeningRepository(AuctionDBContext context)
        {
            _context = context;
        }
        public void Add(LotListening entity)
        {
            _context.Listenings.Add(entity);
        }

        public void DeleteById(int id)
        {
            LotListening listening = _context.Listenings.Local.FirstOrDefault(l => l.Id == id);
            if (listening is null)
            {
                _context.Listenings.Remove(new LotListening { Id = id });
            }
            else
            {
                _context.Listenings.Remove(listening);
            }

        }
        public void Delete(LotListening entity)
        {
            _context.Listenings.Remove(entity);
        }

        public async Task<IEnumerable<LotListening>> GetAllAsync()
        {
            return await _context.Listenings.ToListAsync();
        }

        public async Task<LotListening> GetByIdAsync(int id)
        {
            return await _context.Listenings.FindAsync(id);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(LotListening entity)
        {
            _context.Listenings.Update(entity);
        }
    }
}

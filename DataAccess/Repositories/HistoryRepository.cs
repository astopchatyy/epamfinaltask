﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class HistoryRepository : IHistoryRepository
    {
        private readonly AuctionDBContext _context;
        public HistoryRepository(AuctionDBContext context)
        {
            _context = context;
        }
        public void Add(BetHistory entity)
        {
            _context.Histories.Add(entity);
        }

        public void DeleteById(int id)
        {
            BetHistory history = _context.Histories.Local.FirstOrDefault(l => l.Id == id);
            if (history is null)
            {
                _context.Histories.Remove(new BetHistory { Id = id });
            }
            else
            {
                _context.Histories.Remove(history);
            }

        }
        public void Delete(BetHistory entity)
        {
            _context.Histories.Remove(entity);
        }

        public async Task<IEnumerable<BetHistory>> GetAllAsync()
        {
            return await _context.Histories.ToListAsync();
        }

        public async Task<BetHistory> GetByIdAsync(int id)
        {
            return await _context.Histories.FindAsync(id);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(BetHistory entity)
        {
            _context.Histories.Update(entity);
        }
    }
}

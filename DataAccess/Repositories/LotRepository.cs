﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class LotRepository : ILotRepository
    {
        private readonly AuctionDBContext _context;
        public LotRepository(AuctionDBContext context)
        {
            _context = context;
        }
        public void Add(Lot entity)
        {
            _context.Lots.Add(entity);
        }

        public void DeleteById(int id)
        {
            Lot lot = _context.Lots.Local.FirstOrDefault(l => l.Id == id);
            if(lot is null)
            { 
                _context.Lots.Remove(new Lot { Id = id }); 
            }
            else
            {
                _context.Lots.Remove(lot);
            }
            
        }
        public void Delete(Lot lot)
        {
            _context.Lots.Remove(lot);
        }

        public async Task<IEnumerable<Lot>> GetAllAsync()
        {
            return await _context.Lots.ToListAsync();
        }

        public async Task<Lot> GetByIdAsync(int id)
        {
            return await _context.Lots.FindAsync(id);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(Lot entity)
        {
            _context.Lots.Update(entity);
        }
    }
}

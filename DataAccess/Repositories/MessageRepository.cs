﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly AuctionDBContext _context;
        public MessageRepository(AuctionDBContext context)
        {
            _context = context;
        }
        public void Add(Message entity)
        {
            _context.Messages.Add(entity);
        }

        public void DeleteById(int id)
        {
            Message message = _context.Messages.Local.FirstOrDefault(l => l.Id == id);
            if (message is null)
            {
                _context.Messages.Remove(new Message { Id = id });
            }
            else
            {
                _context.Messages.Remove(message);
            }

        }
        public void Delete(Message entity)
        {
            _context.Messages.Remove(entity);
        }

        public async Task<IEnumerable<Message>> GetAllAsync()
        {
            return await _context.Messages.ToListAsync();
        }

        public async Task<Message> GetByIdAsync(int id)
        {
            return await _context.Messages.FindAsync(id);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Update(Message entity)
        {
            _context.Messages.Update(entity);
        }
    }
}

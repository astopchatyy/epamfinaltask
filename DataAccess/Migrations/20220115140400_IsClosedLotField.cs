﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class IsClosedLotField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "582c5245-a6ae-4ec8-882f-a5185527cddd");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "d21e98fa-e594-400d-9e8c-17a355a4bbe1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "fd294ec1-625e-40d4-8c6e-9139c51fb9bb");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "9f135ecf-b25d-4d6c-abcc-cc92a30c6003", "b81134b0-31b1-48ad-b40f-79ef46a3ccaf", "user", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "677a9cad-6835-4eb7-b5a9-401ed89ad7da", "c7344e12-4945-4d30-976e-0e87088c1c15", "admin", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "07ff01fa-8e09-4908-ab25-d9895c1f1f36", "a693a141-05fd-4868-be16-39e1a8dc3e39", "moderator", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "07ff01fa-8e09-4908-ab25-d9895c1f1f36");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "677a9cad-6835-4eb7-b5a9-401ed89ad7da");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9f135ecf-b25d-4d6c-abcc-cc92a30c6003");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d21e98fa-e594-400d-9e8c-17a355a4bbe1", "34b0b7dd-94f2-4787-9416-cebaeb143634", "user", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "fd294ec1-625e-40d4-8c6e-9139c51fb9bb", "759fb4fb-2402-4486-a33a-83d08faad93b", "admin", null });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "582c5245-a6ae-4ec8-882f-a5185527cddd", "cb2fabe5-bb2d-4c0e-b346-b19a6223f10a", "moderator", null });
        }
    }
}

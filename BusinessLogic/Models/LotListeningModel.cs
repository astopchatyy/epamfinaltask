﻿namespace BusinessLogic.Models
{
    public class LotListeningModel
    {
        public string UserName { get; set; }
        public int LotId { get; set; }
    }
}

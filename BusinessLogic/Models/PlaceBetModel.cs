﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class PlaceBetModel
    {
        [Required]
        public int LotId { get; set; }
        [Required]
        public decimal NewPrice { get; set; }
    }
}

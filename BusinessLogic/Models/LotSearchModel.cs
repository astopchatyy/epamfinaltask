﻿namespace BusinessLogic.Models
{
    public class LotSearchModel
    {
        public string OwnerUserName { get; set; }
        public string Title { get; set; }
    }
}

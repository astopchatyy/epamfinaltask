﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class PaymentModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public decimal Amount { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class LotCreateModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }


        [Required]
        public decimal StartPrice { get; set; }
        [Required]
        public decimal PriceStep { get; set; }
        [Required]
        public decimal MaxPriceForProlong { get; set; }

        public DateTime ClosingTime { get; set; }
        public double ProlongValue { get; set; }
    }
}

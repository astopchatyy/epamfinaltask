﻿using System;

namespace BusinessLogic.Models
{
    public class LotForListModel
    {
        public int LotId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string OwnerUserName { get; set; }
        public DateTime ClosingTime { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal MinNextPrice { get; set; }
        public bool ListeningAllowed { get; set; }
    }
}

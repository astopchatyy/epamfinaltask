﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class RegistrationModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string PasswordConfirmation { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

    }
}

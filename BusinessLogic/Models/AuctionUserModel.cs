﻿using System.Collections.Generic;

namespace BusinessLogic.Models
{
    public class AuctionUserModel
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal TotalBalance { get; set; }

        public List<string> Roles { get; set; }
    }
}

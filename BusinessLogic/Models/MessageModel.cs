﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class MessageModel
    {
        public int MessageId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Text { get; set; }

        public DateTime Time { get; set; }

        public bool IsRead { get; set; }
    }
}

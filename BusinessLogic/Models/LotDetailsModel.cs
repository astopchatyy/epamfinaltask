﻿using System;
using System.Collections.Generic;

namespace BusinessLogic.Models
{
    public class LotDetailsModel
    {
        public int LotId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string OwnerUserName { get; set; }
        public DateTime ClosingTime { get; set; }
        public bool Prolongable { get; set; }
        public bool IsClosed { get; set; }
        public bool ListeningAllowed { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal MinNextPrice { get; set; }
        public ICollection<BetHistoryModel> BetHistory { get; set; }
    }
}

﻿using System;

namespace BusinessLogic.Models
{
    public class BetHistoryModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime BetTime { get; set; }
        public decimal Price { get; set; }
    }
}

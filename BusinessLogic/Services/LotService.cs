﻿using AutoMapper;
using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class LotService : ILotService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INotifyService _notifier;
        private readonly ILotClosingService _lotClosingService;
        private readonly IValidationService<LotCreateModel> _modelValidationService;

        public LotService(IUnitOfWork unitOfWork, IMapper mapper, INotifyService notifier,
            IValidationService<LotCreateModel> createModelValidator, ILotClosingService lotClosingService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _modelValidationService = createModelValidator;
            _notifier = notifier;
            _lotClosingService = lotClosingService;
        }
        public async Task CreateLotAsync(LotCreateModel model)
        {
            _modelValidationService.Validate(model);
            Lot mappedModel = _mapper.Map<Lot>(model);
            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(model.UserName);
            if (user is null)
                throw new LotModelException("Unauthorized");
            mappedModel.User = user;
            _unitOfWork.LotRepository.Add(mappedModel);
            await _unitOfWork.SaveAllChangesAsync();

        }

        public async Task DeleteLotByIdAsync(int id)
        {
            Lot lot = await _unitOfWork.LotRepository.GetByIdAsync(id);
            if(lot.LotHistory != null && lot.LotHistory.Count()> 0)
            {
                BetHistory lastBet = lot.LotHistory.OrderByDescending(h => h.BetTime).First();
                AuctionUser lastChallenger = lastBet.User;
                lastChallenger.CurrentBalance += lastBet.Price;
                
            }
            foreach (var history in lot.LotHistory)
            {
                _unitOfWork.HistoryRepository.Delete(history);
            }
            foreach (var listener in lot.Listeners)
            {
                _unitOfWork.ListeningRepository.Delete(listener);
            }

            _unitOfWork.LotRepository.Delete(lot);
            await _unitOfWork.SaveAllChangesAsync();
        }

        public async Task<IEnumerable<LotForListModel>> GetListenedLotListByUserAsync(string username, LotSearchModel searchModel)
        {

            IEnumerable<Lot> findedLots = await GetLotListAsync(searchModel);

            IEnumerable<Lot> findedLotsByListener = findedLots.Where(l => l.Listeners.Any(l => l.User.UserName == username));

            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotForListModel>>(findedLotsByListener);

        }

        public async Task<LotDetailsModel> GetLotDetailsAsync(int id)
        {
            Lot lot = await _unitOfWork.LotRepository.GetByIdAsync(id);

            if (lot is null)
                throw new LotModelException("Lot not found");

            return _mapper.Map<LotDetailsModel>(lot);
        }

        private async Task<IEnumerable<Lot>> GetLotListAsync(LotSearchModel searchModel)
        {
            IEnumerable<Lot> allLots = (await _unitOfWork.LotRepository.GetAllAsync()).Where(lot => !lot.IsClosed);
            IEnumerable<Lot> lotForClosing = allLots.Where(lot => lot.ClosingTime <= System.DateTime.Now);
            foreach(Lot lot in lotForClosing)
            {
                await _lotClosingService.CloseLotAsync(lot);
            }
            allLots = allLots.Where(lot => !lot.IsClosed);
            if (searchModel is null || searchModel.OwnerUserName is null && searchModel.Title is null)
            {
                return allLots;
            }
            Regex ownerExpression = new Regex(".*" + searchModel.OwnerUserName ?? "" + ".*");
            Regex titleExpression = new Regex(".*" + searchModel.Title ?? "" + ".*");

            if(searchModel.OwnerUserName is null || searchModel.Title is null)
                return allLots.Where(lot =>
            ownerExpression.Matches(lot.User.UserName).Count > 0 && titleExpression.Matches(lot.Title).Count > 0);
            return allLots.Where(lot =>
            ownerExpression.Matches(lot.User.UserName).Count > 0 || titleExpression.Matches(lot.Title).Count > 0);
        }


        public async Task<IEnumerable<LotForListModel>> GetLotModelListAsync(LotSearchModel searchModel)
        {
            IEnumerable<Lot> findedLots = await GetLotListAsync(searchModel);

            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotForListModel>>(findedLots);
        }

        public async Task<IEnumerable<LotForListModel>> GetLotListByUserAsync(string username, LotSearchModel searchModel)
        {
            IEnumerable<Lot> findedLots = await GetLotListAsync(searchModel);

            IEnumerable<Lot> findedLotsForUser = findedLots.Where(l => l.User.UserName == username);

            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotForListModel>>(findedLotsForUser);
        }
    }
}

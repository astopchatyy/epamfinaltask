﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using System;

namespace BusinessLogic.Services
{
    public class BetMessageBuilder : IBetMessageBuilder
    {
        public MessageModel BuildMessageForChallenger(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("Your bet is outbid");

            string text = String.Format("{0} was outbid your bet on lot {1} with {2}", challenger.UserName, lot.Title, lot.CurrentPrice);

            return new MessageModel { Title = title, Text = text , Time = DateTime.Now};
        }

        public MessageModel BuildMessageForListener(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("New bet on lot {0}", lot.Title);

            string text = String.Format("{0} made a bet of {2} on lot {1}", challenger.UserName, lot.Title, lot.CurrentPrice);

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }

        public MessageModel BuildMessageForOwner(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("New bet on your lot {0}", lot.Title);

            string text = String.Format("{0} made a bet of {2} on your lot {1}", challenger.UserName, lot.Title, lot.CurrentPrice);

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }
    }
}

﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class LotListeningService : ILotListeningService
    {
        private readonly IUnitOfWork _unitOfWork;
        public LotListeningService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddListener(LotListeningModel listener)
        {
            if (listener is null)
                throw new LotListeningException("Listening data is empty");
            if (listener.UserName is null)
                throw new LotListeningException("Listening data is empty");

            Lot lot = await _unitOfWork.LotRepository.GetByIdAsync(listener.LotId);

            if(lot.IsClosed)
                throw new LotListeningException("Lot was closed");

            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(listener.UserName);

            if (!lot.Listeners.Any(l => l.User == user))
            {
                LotListening listening = new LotListening { Lot = lot, User = user };

                _unitOfWork.ListeningRepository.Add(listening);
                await _unitOfWork.ListeningRepository.SaveChangesAsync();
            }
        }

        public async Task AllowListening(string userName, IEnumerable<LotForListModel> lots)
        {
            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(userName);
            foreach (LotForListModel lot in lots)
            {
                lot.ListeningAllowed = checkLot(lot.OwnerUserName, user, lot.LotId);
            }
        }
        private bool checkLot(string ownerUserName, AuctionUser user, int lotId)
        {
            return ownerUserName != user.UserName && !user.ListenedLots.Any(l => l.LotId == lotId);
        }

        public async Task AllowListening(string userName, LotDetailsModel lot)
        {
            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(userName);
            lot.ListeningAllowed = checkLot(lot.OwnerUserName, user, lot.LotId);
        }

        public async Task RemoveListener(LotListeningModel listener)
        {
            if (listener is null)
                throw new LotListeningException("Listener data is empty");
            if (listener.UserName is null)
                throw new LotListeningException("Listener data is empty");

            Lot lot = await _unitOfWork.LotRepository.GetByIdAsync(listener.LotId);

            if (lot is null)
                throw new LotListeningException("Lot was closed");

            LotListening listening = lot.Listeners.Where(l => l.User.UserName == listener.UserName).FirstOrDefault();

            if (listening != null)
            {
                _unitOfWork.ListeningRepository.DeleteById(listening.Id);
                await _unitOfWork.ListeningRepository.SaveChangesAsync();
            }
        }
    }
}

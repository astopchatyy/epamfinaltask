﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using System;

namespace BusinessLogic.Services
{
    public class ClosingMessageBuilder : IClosingMessageBuilder
    {
        public MessageModel BuildSalesMessageForChallenger(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("Your bet wins");

            string text = String.Format("  ");//TODO

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }

        public MessageModel BuildSalesMessageForListener(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("Bidding for the lot {0} has ended", lot.Title);

            string text = String.Format(" "); //TODO

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }

        public MessageModel BuildSalesMessageForOwner(AuctionUser challenger, Lot lot)
        {
            string title = String.Format("Bidding for your lot {0} has ended", lot.Title);

            string text = String.Format(" "); //TODO

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }

        public MessageModel BuildNotSoldMessageForListener(Lot lot)
        {
            string title = String.Format("Bidding for the lot {0} has ended", lot.Title);

            string text = String.Format(" "); //TODO

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }

        public MessageModel BuildNotSoldMessageForOwner(Lot lot)
        {
            string title = String.Format("Bidding for your lot {0} has ended", lot.Title);

            string text = String.Format(" "); //TODO

            return new MessageModel { Title = title, Text = text, Time = DateTime.Now };
        }
    }
}

﻿using AutoMapper;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IValidationService<MessageModel> _modelValidationService;

        public MessageService(IUnitOfWork unitOfWork, IMapper mapper, IValidationService<MessageModel> messageModelValidator)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _modelValidationService = messageModelValidator;
        }

        public async Task CreateMessageAsync(string userId, MessageModel message)
        {
            _modelValidationService.Validate(message);

            Message mappedMessage = _mapper.Map<Message>(message);

            mappedMessage.UserId = userId;

            _unitOfWork.MessageRepository.Add(mappedMessage);
            await _unitOfWork.MessageRepository.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            _unitOfWork.MessageRepository.DeleteById(id);
            await _unitOfWork.MessageRepository.SaveChangesAsync();
        }

        public async Task<MessageModel> GetByIdAsync(int id)
        {
            Message message = await _unitOfWork.MessageRepository.GetByIdAsync(id);
            return _mapper.Map<MessageModel>(message); 
        }

        public async Task<IEnumerable<MessageModel>> GetMessagesByUserNameAsync(string userName)
        {
            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(userName);

            IEnumerable<Message> messages = user.Messages;

            IEnumerable<MessageModel> mappedMessages = _mapper
                .Map<IEnumerable<Message>, IEnumerable<MessageModel>>(messages);

            return mappedMessages;
        }

        public async Task MarkAsReadAsync(int messageId)
        {

            Message message = await _unitOfWork.MessageRepository.GetByIdAsync(messageId);

            message.Stat = (int)MessageStat.Read;

            _unitOfWork.MessageRepository.Update(message);
            await _unitOfWork.SaveAllChangesAsync();
        }
    }
}

﻿using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class NotifyService : INotifyService
    {
        private readonly IMessageService _messageService;

        public NotifyService(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public async Task Notify(List<AuctionUser> users, MessageModel message)
        {
            foreach(var user in users)
            {
                await Notify(user, message);
            }
        }

        public async Task Notify(AuctionUser user, MessageModel message)
        {
            await _messageService.CreateMessageAsync(user.Id, message);
        }
    }
}

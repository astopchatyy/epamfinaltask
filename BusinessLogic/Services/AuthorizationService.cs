﻿using AutoMapper;
using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IJwtStringBuilder _jwtBuilder;
        private readonly IMapper _mapper;
        private readonly IValidationService<RegistrationModel> _modelValidationService;

        public AuthorizationService(IUnitOfWork unitOfWork, IJwtStringBuilder jwtBuilder,
            IValidationService<RegistrationModel> modelValidationService, IMapper mapper)
        {
            _mapper = mapper;
            _jwtBuilder = jwtBuilder;
            _unitOfWork = unitOfWork;
            _modelValidationService = modelValidationService;
        }

        private async Task<ClaimsIdentity> GetClaimsAsync(AuctionUser user)
        {
            IEnumerable<string> userRoles = await _unitOfWork.UserManager.GetRolesAsync(user);
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };
            claims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));

            return new ClaimsIdentity(claims);
        }

        private async Task<string> GetJwtTokenAsync(AuctionUser user)
        {
            return _jwtBuilder.Build( await GetClaimsAsync(user));
        }

        public async Task<AuctionUserModel> LogInAsync(LogInModel model)
        {
            if (model is null)
                throw new LoginModelException("Login data is empty");

            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(model.UserName);

            if (user is null)
                throw new LoginModelException("Wrong login");
            if(user.PasswordHash != model.Password)
                throw new LoginModelException("Wrong password");

            AuctionUserModel mappedUser = _mapper.Map<AuctionUserModel>(user);

            mappedUser.Roles = (await _unitOfWork.UserManager.GetRolesAsync(user)).ToList();
            mappedUser.Token = await GetJwtTokenAsync(user);

            return mappedUser;
        }

        public async Task<AuctionUserModel> RegisterAsync(RegistrationModel model)
        {
            _modelValidationService.Validate(model);

            AuctionUser user = _mapper.Map<AuctionUser>(model);

            var result = await _unitOfWork.UserManager
                .CreateAsync(user);
            if(!result.Succeeded)
                throw new RegistrationModelException(result.ToString());

            if (!(await _unitOfWork.RoleManager.RoleExistsAsync("user")))
                await _unitOfWork.RoleManager.CreateAsync(new IdentityRole("user"));

            result =  await _unitOfWork.UserManager.AddToRoleAsync(user, "user");

            if (!result.Succeeded)
                throw new RegistrationModelException(result.ToString());

             AuctionUserModel mappedUser = _mapper.Map<AuctionUserModel>(user);

            return mappedUser;
        }


    }
}

﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AccountManagmentService : IAccountManagmentService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AccountManagmentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task PayAsync(PaymentModel payment)
        {
            if (payment is null)
                throw new PaymentException("Payment data is empty");

            if (payment.Amount < 0)
                throw new PaymentException("Payment amount can`t be negative");

            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(payment.UserName);

            if(user is null)
                throw new PaymentException("Unauthorized");

            user.CurrentBalance += payment.Amount;
            user.TotalBalance += payment.Amount;

            await _unitOfWork.UserManager.UpdateAsync(user);
        }

        public async Task WithdrawAsync(PaymentModel payment)
        {
            if (payment is null)
                throw new PaymentException("Payment data is empty");

            if (payment.Amount < 0)
                throw new PaymentException("Payment amount can`t be negative");

            AuctionUser user = await _unitOfWork.UserManager.FindByNameAsync(payment.UserName);

            if (user is null)
                throw new PaymentException("Unauthorized");

            if (user.CurrentBalance < payment.Amount)
                throw new PaymentException("Not enough balance");

            user.CurrentBalance -= payment.Amount;
            user.TotalBalance -= payment.Amount;

            await _unitOfWork.UserManager.UpdateAsync(user);
        }
    }
}

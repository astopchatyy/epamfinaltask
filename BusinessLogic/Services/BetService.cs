﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class BetService : IBetService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifier;
        private readonly ILotListeningService _listeningService;
        private readonly IBetMessageBuilder _messageBuilder;

        public BetService(IUnitOfWork unitOfWork, INotifyService notifier,
            IBetMessageBuilder messageBuilder, ILotListeningService listeningService)
        {
            _unitOfWork = unitOfWork;
            _notifier = notifier;
            _messageBuilder = messageBuilder;
            _listeningService = listeningService;
        }

        public async Task PlaceBetAsync(string userName, PlaceBetModel model)
        {
            if (model is null)
                throw new BetException("Bet data is empty");

            Lot lot = await _unitOfWork.LotRepository.GetByIdAsync(model.LotId);

            if (lot is null || lot.IsClosed)
                throw new BetException("Lot was closed");

            if(lot.User.UserName == userName)
                throw new BetException("You can`t place a bet on your own lot");

            IEnumerable<BetHistory> lotHistory = (await _unitOfWork.HistoryRepository.GetAllAsync()).Where(h => h.LotId == model.LotId);

            AuctionUser challenger = await _unitOfWork.UserManager.FindByNameAsync(userName);

            if (challenger is null)
                throw new BetException("Unauthorized");

            if (challenger.CurrentBalance < model.NewPrice)
                throw new BetException("Not enough balance");

            if(model.NewPrice < lot.CurrentPrice)
                throw new BetException("You can`t cut the price");

            AuctionUser lastChallenger = null;
            if (!(lotHistory is null) && lotHistory.Count() > 0)
            {
                BetHistory lastBet = lotHistory.OrderByDescending(h => h.BetTime).First();
                lastChallenger = lastBet.User;
                lastChallenger.CurrentBalance += lastBet.Price;
            }

            await _notifier.Notify(lot.Listeners.Select(ll => ll.User).Where(u => u != lastChallenger ).ToList(), _messageBuilder.BuildMessageForListener(challenger, lot));
            await _notifier.Notify(lot.User, _messageBuilder.BuildMessageForOwner(challenger, lot));

            BetHistory newBet = new BetHistory { Lot = lot, User = challenger, BetTime = DateTime.Now, Price = model.NewPrice };

            _unitOfWork.HistoryRepository.Add(newBet);

            challenger.CurrentBalance -= model.NewPrice;

            lot.CurrentPrice = model.NewPrice;

            if (lot.CurrentPrice <= lot.MaxPriceForProlong && lot.ClosingTime <= DateTime.Now.AddMinutes(lot.ProlongValue))
            {
                lot.ClosingTime = lot.ClosingTime.AddMinutes(lot.ProlongValue);
            }

            _unitOfWork.LotRepository.Update(lot);
            await _unitOfWork.SaveAllChangesAsync();

            await _listeningService.AddListener(new LotListeningModel { LotId = lot.Id, UserName = challenger.UserName });
        }

        public async Task RemoveBetByIdAsync(int betId)
        {
            BetHistory bet = await _unitOfWork.HistoryRepository.GetByIdAsync(betId);
            Lot lot = bet.Lot;
            BetHistory lastBet = lot.LotHistory.OrderByDescending(h => h.BetTime).First();
            if (betId == lastBet.Id)
            {
                AuctionUser lastChallenger = lastBet.User;
                lastChallenger.CurrentBalance += lastBet.Price;
            }
            _unitOfWork.HistoryRepository.Delete(bet);
            await _unitOfWork.HistoryRepository.SaveChangesAsync();
        }
    }
}

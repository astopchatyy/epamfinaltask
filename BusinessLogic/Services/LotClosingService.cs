﻿using BusinessLogic.Interfaces;
using DataAccess.Entities;
using DataAccess.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class LotClosingService : ILotClosingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotifyService _notifier;
        private readonly IClosingMessageBuilder _messageBuilder;

        public LotClosingService(IUnitOfWork unitOfWork, INotifyService notifier,
            IClosingMessageBuilder messageBuilder)
        {
            _unitOfWork = unitOfWork;
            _notifier = notifier;
            _messageBuilder = messageBuilder;
        }

        public async Task CloseLotAsync(Lot lot)
        {
            lot.IsClosed = true;
            _unitOfWork.LotRepository.Update(lot);
            await _unitOfWork.LotRepository.SaveChangesAsync();
            

            AuctionUser lastChallenger = null;
            if (!(lot.LotHistory is null) && lot.LotHistory.Count() > 0)
            {
                BetHistory lastBet = lot.LotHistory.OrderByDescending(h => h.BetTime).First();
                lastChallenger = lastBet.User;
                lastChallenger.TotalBalance -= lastBet.Price;
                lot.User.TotalBalance += lastBet.Price;
                lot.User.CurrentBalance += lastBet.Price;

                await _notifier.Notify(lastChallenger, _messageBuilder.BuildSalesMessageForChallenger(lastChallenger, lot));
                await _notifier.Notify(lot.Listeners.Select(ll => ll.User).Where(u => u != lastChallenger).ToList(),
                    _messageBuilder.BuildSalesMessageForListener(lastChallenger, lot));
                await _notifier.Notify(lot.User, _messageBuilder.BuildSalesMessageForChallenger(lastChallenger, lot));
            }
            else
            {
                await _notifier.Notify(lot.Listeners.Select(ll => ll.User).ToList(),
      _messageBuilder.BuildNotSoldMessageForListener(lot));
                await _notifier.Notify(lot.User, _messageBuilder.BuildNotSoldMessageForOwner(lot));
            }
        }

            
    }
}

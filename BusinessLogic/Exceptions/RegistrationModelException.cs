﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    public class RegistrationModelException : ArgumentException
    {
        public RegistrationModelException()
        {
        }

        public RegistrationModelException(string message) : base(message)
        {
        }

        public RegistrationModelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public RegistrationModelException(string message, string paramName) : base(message, paramName)
        {
        }

        public RegistrationModelException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected RegistrationModelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

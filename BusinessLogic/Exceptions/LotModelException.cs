﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    public class LotModelException : ArgumentException
    {
        public LotModelException()
        {
        }

        public LotModelException(string message) : base(message)
        {
        }

        public LotModelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LotModelException(string message, string paramName) : base(message, paramName)
        {
        }

        public LotModelException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected LotModelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    public class PaymentException : ArgumentException
    {
        public PaymentException()
        {
        }

        public PaymentException(string message) : base(message)
        {
        }

        public PaymentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public PaymentException(string message, string paramName) : base(message, paramName)
        {
        }

        public PaymentException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected PaymentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

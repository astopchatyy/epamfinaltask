﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    public class BetException : ArgumentException
    {
        public BetException()
        {
        }

        public BetException(string message) : base(message)
        {
        }

        public BetException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BetException(string message, string paramName) : base(message, paramName)
        {
        }

        public BetException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected BetException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

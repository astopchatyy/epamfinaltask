﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    internal class LotListeningException : ArgumentException
    {
        public LotListeningException()
        {
        }

        public LotListeningException(string message) : base(message)
        {
        }

        public LotListeningException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LotListeningException(string message, string paramName) : base(message, paramName)
        {
        }

        public LotListeningException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected LotListeningException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

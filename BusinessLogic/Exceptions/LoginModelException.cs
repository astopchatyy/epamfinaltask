﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    public class LoginModelException : ArgumentException
    {
        public LoginModelException()
        {
        }

        public LoginModelException(string message) : base(message)
        {
        }

        public LoginModelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LoginModelException(string message, string paramName) : base(message, paramName)
        {
        }

        public LoginModelException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected LoginModelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

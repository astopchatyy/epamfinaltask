﻿using System;
using System.Runtime.Serialization;

namespace BusinessLogic.Exceptions
{
    internal class MessageModelException : ArgumentException
    {
        public MessageModelException()
        {
        }

        public MessageModelException(string message) : base(message)
        {
        }

        public MessageModelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public MessageModelException(string message, string paramName) : base(message, paramName)
        {
        }

        public MessageModelException(string message, string paramName, Exception innerException) : base(message, paramName, innerException)
        {
        }

        protected MessageModelException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

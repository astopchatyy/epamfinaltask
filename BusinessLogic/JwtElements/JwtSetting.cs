﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace BusinessLogic.JwtElements
{
    public class JwtSetting
    {
        public string Issuer { get; } = "AuctionWebAPi";
        public string Audience { get; } = "AuctionAngClient";
        public string EncriptionKey { get; } = "Fort-nox_secretKey1404";
        public SymmetricSecurityKey SymmetricSecurityKey { get; }
        public JwtSetting()
        {
            SymmetricSecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(EncriptionKey));
        }
    }
}

﻿using BusinessLogic.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace BusinessLogic.JwtElements
{
    public class JwtStringBuilder : IJwtStringBuilder
    {
        public string Build(ClaimsIdentity claims, long expTimeInSeconds)
        {
            JwtSetting setting = new JwtSetting();
            JwtSecurityToken rawToken = new JwtSecurityToken(setting.Issuer, setting.Audience,
                claims.Claims, DateTime.Now, DateTime.Now.AddSeconds(expTimeInSeconds),
                new SigningCredentials(setting.SymmetricSecurityKey, SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(rawToken);
        }

        public string Build(ClaimsIdentity claims)
        {
            return Build(claims, 18000);
        }
    }
}

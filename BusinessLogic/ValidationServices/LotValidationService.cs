﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BusinessLogic.ValidationServices
{
    public class LotValidationService : IValidationService<LotCreateModel>
    {
        public const long MinDuration = 1;
        public void Validate(LotCreateModel model)
        {
            var validationResult = new List<ValidationResult>();
            var validationContext = new ValidationContext(model);
            if (!Validator.TryValidateObject(model, validationContext, validationResult))
            {
                throw new LotModelException(validationResult.First().ErrorMessage);
            }

            if(model.ClosingTime <= DateTime.Now.AddMinutes(MinDuration))
            {
                throw new LotModelException("Wrong lot closing time");
            }
            if (model.ProlongValue < 0)
            {
                throw new LotModelException("Wrong lot closing time prolong value");
            }
        }
    }
}

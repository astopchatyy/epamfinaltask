﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BusinessLogic.ValidationServices
{
    public class RegistrationValidationService : IValidationService<RegistrationModel>
    {
        public void Validate(RegistrationModel model)
        {
            var validationResult = new List<ValidationResult>();
            var validationContext = new ValidationContext(model);
            if (!Validator.TryValidateObject(model, validationContext, validationResult))
            {
                throw new RegistrationModelException(validationResult.First().ErrorMessage);
            }
            if(model.Password != model.PasswordConfirmation)
            {
                throw new RegistrationModelException("The password doesn`t match it`s confirmation ");
            }
        }
    }
}

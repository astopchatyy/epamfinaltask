﻿using BusinessLogic.Exceptions;
using BusinessLogic.Interfaces;
using BusinessLogic.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BusinessLogic.ValidationServices
{
    public class MessageValidationService : IValidationService<MessageModel>
    {
        public void Validate(MessageModel model)
        {
            var validationResult = new List<ValidationResult>();
            var validationContext = new ValidationContext(model);
            if (!Validator.TryValidateObject(model, validationContext, validationResult))
            {
                throw new MessageModelException(validationResult.First().ErrorMessage);
            }
        }
    }
}

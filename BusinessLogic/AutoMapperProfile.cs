﻿using AutoMapper;
using BusinessLogic.Models;
using DataAccess.Entities;
using System;

namespace BusinessLogic
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<MessageModel, Message>()
                .ForMember(m => m.Id, c => c.MapFrom(m => m.MessageId))
                .ForMember(m => m.Stat, c=> c.MapFrom(m => m.IsRead ? MessageStat.Read : MessageStat.New));

            CreateMap<Message, MessageModel>()
                .ForMember(m => m.MessageId, c => c.MapFrom(m => m.Id))
                .ForMember(m => m.IsRead, c => c.MapFrom(m => m.Stat));

            CreateMap<RegistrationModel, AuctionUser>()
                .ForMember(u => u.UserName, c => c.MapFrom(m => m.UserName))
                .ForMember(u => u.PasswordHash, c => c.MapFrom(m => m.Password));

            CreateMap<LotCreateModel, Lot>()
                .ForMember(l => l.CurrentPrice, c => c.MapFrom(m => m.StartPrice))
                .ForMember(l => l.Created, c => c.MapFrom(m => DateTime.Now))
                .ForMember(l => l.IsClosed, c => c.MapFrom(m => false));

            CreateMap<Lot, LotForListModel>()
                .ForMember(m => m.LotId, c => c.MapFrom(l => l.Id))
                .ForMember(m => m.OwnerUserName, c => c.MapFrom(l => l.User.UserName))
                .ForMember(m => m.CurrentPrice, c => c.MapFrom(l => l.CurrentPrice))
                .ForMember(m => m.MinNextPrice, c => c.MapFrom(l => l.CurrentPrice + l.PriceStep))
                .ForMember(m => m.Description, c => c.MapFrom(l => 
                l.Description.Length > 50 ? l.Description.Substring(0, 47) + "...": l.Description));


            CreateMap<BetHistory, BetHistoryModel>()
                .ForMember(m => m.UserName, c => c.MapFrom(h => h.User.UserName))
                .ForMember(m => m.Price, c => c.MapFrom(h => h.Price))
                .ForMember(m => m.BetTime, c => c.MapFrom(h => h.BetTime));

            CreateMap<Lot, LotDetailsModel>()
                .ForMember(m => m.LotId, c => c.MapFrom(l => l.Id))
                .ForMember(m => m.Prolongable, c => c.MapFrom(l => l.CurrentPrice + l.PriceStep < l.MaxPriceForProlong))
                .ForMember(m => m.BetHistory, c => c.MapFrom(l => l.LotHistory))
                .ForMember(m => m.MinNextPrice, c => c.MapFrom(l => l.CurrentPrice + l.PriceStep))
                .ForMember(m => m.OwnerUserName, c => c.MapFrom(l => l.User.UserName));

            CreateMap<AuctionUser, AuctionUserModel>()
                .ReverseMap();
        }
    }
}

﻿using BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface ILotService
    {
        Task CreateLotAsync(LotCreateModel model);
        Task<IEnumerable<LotForListModel>> GetLotModelListAsync(LotSearchModel searchModel);
        Task<LotDetailsModel> GetLotDetailsAsync(int id);
        Task<IEnumerable<LotForListModel>> GetLotListByUserAsync(string username, LotSearchModel searchModel);
        Task<IEnumerable<LotForListModel>> GetListenedLotListByUserAsync(string username, LotSearchModel searchModel);
        Task DeleteLotByIdAsync(int id);
    }
}

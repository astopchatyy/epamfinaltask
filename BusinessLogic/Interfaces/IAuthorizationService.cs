﻿using BusinessLogic.Models;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IAuthorizationService
    {
        Task<AuctionUserModel> RegisterAsync(RegistrationModel model);
        Task<AuctionUserModel> LogInAsync(LogInModel model);
    }
}

﻿using BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface ILotListeningService
    {
        Task AddListener(LotListeningModel listener);

        Task RemoveListener(LotListeningModel listener);

        Task AllowListening(string userName, IEnumerable<LotForListModel> lots);
        Task AllowListening(string userName, LotDetailsModel lot);
    }
}

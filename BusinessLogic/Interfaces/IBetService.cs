﻿using BusinessLogic.Models;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IBetService
    {
        Task PlaceBetAsync(string username, PlaceBetModel model);
        Task RemoveBetByIdAsync(int betId);
    }
}

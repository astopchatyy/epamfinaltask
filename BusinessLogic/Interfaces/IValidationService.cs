﻿namespace BusinessLogic.Interfaces
{
    public interface IValidationService<T> where T: class
    {
        void Validate(T Model);
    }
}

﻿using BusinessLogic.Models;
using DataAccess.Entities;

namespace BusinessLogic.Interfaces
{
    public interface IClosingMessageBuilder
    {
        MessageModel BuildSalesMessageForListener(AuctionUser challenger, Lot lot);
        MessageModel BuildSalesMessageForChallenger(AuctionUser challenger, Lot lot);
        MessageModel BuildSalesMessageForOwner(AuctionUser challenger, Lot lot);

        MessageModel BuildNotSoldMessageForListener(Lot lot);
        MessageModel BuildNotSoldMessageForOwner(Lot lot);
    }
}

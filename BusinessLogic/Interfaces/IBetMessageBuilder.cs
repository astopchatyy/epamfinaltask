﻿using BusinessLogic.Models;
using DataAccess.Entities;

namespace BusinessLogic.Interfaces
{
    public interface IBetMessageBuilder
    {
        MessageModel BuildMessageForListener(AuctionUser challenger, Lot lot);
        MessageModel BuildMessageForChallenger(AuctionUser challenger, Lot lot);
        MessageModel BuildMessageForOwner(AuctionUser challenger, Lot lot);
    }
}

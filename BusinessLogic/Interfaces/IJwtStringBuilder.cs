﻿using System.Security.Claims;

namespace BusinessLogic.Interfaces
{
    public interface IJwtStringBuilder
    {
        string Build(ClaimsIdentity claims);
        string Build(ClaimsIdentity claims, long expTimeInSeconds);
    }
}

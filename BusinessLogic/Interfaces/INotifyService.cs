﻿using BusinessLogic.Models;
using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface INotifyService
    {
        public Task Notify(List<AuctionUser> users, MessageModel message);
        public Task Notify(AuctionUser user, MessageModel message);
    }
}

﻿using BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IMessageService
    {
        Task CreateMessageAsync(string userId, MessageModel message);

        Task MarkAsReadAsync(int messageId);

        Task<IEnumerable<MessageModel>> GetMessagesByUserNameAsync(string Id);


        Task<MessageModel> GetByIdAsync(int id);

        Task DeleteByIdAsync(int id);
    }
}

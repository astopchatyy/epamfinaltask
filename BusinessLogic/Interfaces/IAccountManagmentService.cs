﻿using BusinessLogic.Models;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface IAccountManagmentService
    {
        Task PayAsync(PaymentModel payment);
        Task WithdrawAsync(PaymentModel payment);
    }
}

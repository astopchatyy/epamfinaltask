﻿using DataAccess.Entities;
using System.Threading.Tasks;

namespace BusinessLogic.Interfaces
{
    public interface ILotClosingService
    {
        Task CloseLotAsync(Lot lot);
    }
}

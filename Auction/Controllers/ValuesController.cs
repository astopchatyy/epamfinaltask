﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DataAccess.Entities;
using DataAccess.Repositories;
using DataAccess;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace AuctionWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        AuctionDBContext _context;
        UserManager<AuctionUser> _userManager;
        public ValuesController(AuctionDBContext context, UserManager<AuctionUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<ActionResult<Message>> Test()
        {
            AuctionUser user = new AuctionUser { UserName = "a" };
            await _userManager.CreateAsync(user);

            Message message = new Message { Text = "t", User = user };
            _context.Messages.Add(message);
            _context.SaveChanges();

            return Ok(await _context.Messages.FirstOrDefaultAsync());
        }
    }
}

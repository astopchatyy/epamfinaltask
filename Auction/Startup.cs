using DataAccess;
using DataAccess.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Interfaces;
using DataAccess.Repositories;
using BusinessLogic.Interfaces;
using BusinessLogic.Services;
using AutoMapper;
using BusinessLogic;
using BusinessLogic.Models;
using BusinessLogic.ValidationServices;

namespace Auction
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AuctionDBContext>(options =>options
            .UseLazyLoadingProxies()
            .UseSqlServer(Configuration.GetConnectionString("AuctionDB")));

            services.AddIdentity<AuctionUser, IdentityRole>()
                .AddEntityFrameworkStores<AuctionDBContext>();

            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<ILotRepository, LotRepository>();
            services.AddScoped<IListeningRepository, ListeningRepository>();
            services.AddScoped<IHistoryRepository, HistoryRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IAuthorizationService, AuthorizationService>();
            services.AddScoped<ILotManagmentService, LotManagmentService>();
            services.AddScoped<IRateService, RateService>();
            services.AddScoped<IMessageService, MessageService>();

            services.AddScoped<IValidationService<RegistrationModel>, AccoutOpsValidationService>();
            services.AddScoped<IValidationService<LogInModel>, AccoutOpsValidationService>();
            services.AddScoped<IValidationService<LotCreateModel>, LotValidationService>();
            services.AddScoped<IValidationService<MessageModel>, MessageValidationService>();

            services.AddScoped<IMapper>(mapper => new Mapper(new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfile()))));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
